﻿using System;
using Client.Enum;

namespace Client.Model
{
    [Serializable]
    public class ServerConfig
    {
        public ServerType serverType;

        public string ip;

        public int port;
    }
}