﻿using System.Collections.Generic;
using Client.Enum;
using Client.Ex;
using Client.Model;
using UnityEngine;

namespace Client.Base
{
    [RequireComponent(typeof(Thread))]
    public abstract class Server : MonoBehaviour
    {
        [SerializeField] protected ServerConfig serverConfig;

        [Tooltip(
            "If autoConnect is true then it will automatically connect to the server in awake without you calling it")]
        [SerializeField]
        private bool autoConnect = true;

        public delegate void ClientHandler(Packet packet);

        public static Dictionary<int, ClientHandler> ClientHandlers { get; private set; } =
            new Dictionary<int, ClientHandler>();

        public static TcpClient TcpClient { get; private set; }

        public static UdpClient UdpClient { get; private set; }

        public static int ClientId { get; private set; }

        #region MonoBehaviour

        private void Awake()
        {
            TcpClient = new TcpClient();
            UdpClient = new UdpClient(serverConfig);

            if (autoConnect)
            {
                Connect();
            }
        }

        private void OnValidate()
        {
            switch (serverConfig.serverType)
            {
                case ServerType.None:
                    Log.Debug($"Server will not be able to run with server type {serverConfig.serverType}");
                    break;

                case ServerType.Local:
                    if (string.IsNullOrEmpty(serverConfig.ip) || string.IsNullOrWhiteSpace(serverConfig.ip))
                    {
                        serverConfig.ip = IpEx.GetLocalIPAddress();
                    }

                    break;
            }
        }

        private void OnApplicationQuit()
        {
            Disconnect();
        }

        #endregion

        #region Public

        public static void Disconnect()
        {
#if UNITY_EDITOR
            if (Application.isEditor || Application.isPlaying)
            {
                ServerEx.Quit();
                return;
            }
#endif

            TcpClient.Disconnect();
            UdpClient.Disconnect();
        }

        #endregion

        #region Private

        private void OnServerStatus(Packet packet)
        {
            var serverCode = (ServerCode) packet.ReadInt();

            switch (serverCode)
            {
                case ServerCode.Connected:
                    ClientId = packet.ReadInt();
                    break;

                case ServerCode.Disconnect:
                    ClientId = 0;

                    Disconnect();
                    break;
            }

            OnServerStatus(serverCode);
        }

        #endregion

        #region Protected

        protected void Connect()
        {
            if (serverConfig.serverType == ServerType.None)
            {
                Log.Debug($"Server can't run with server type {serverConfig.serverType}");
            }
            else
            {
                ClientHandlers = new Dictionary<int, ClientHandler>
                {
                    {
                        1, OnServerStatus
                    }
                };

                TcpClient.Connect(serverConfig);
            }
        }

        protected abstract void OnServerStatus(ServerCode serverCode);

        #endregion
    }
}