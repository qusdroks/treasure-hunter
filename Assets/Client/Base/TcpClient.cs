﻿using System;
using System.Net.Sockets;
using Client.Ex;
using Client.Interface;
using Client.Model;

namespace Client.Base
{
    public class TcpClient : ITcp
    {
        public System.Net.Sockets.TcpClient Socket { get; set; }
        public NetworkStream Stream { get; set; }
        public Packet ReceivedData { get; set; }
        public byte[] ReceiveBuffer { get; set; }

        #region Public

        public void Connect(ServerConfig serverConfig)
        {
            Socket = new System.Net.Sockets.TcpClient
            {
                ReceiveBufferSize = Const.DATA_BUFFER_SIZE,
                SendBufferSize = Const.DATA_BUFFER_SIZE
            };

            ReceiveBuffer = new byte[Const.DATA_BUFFER_SIZE];

            Socket.BeginConnect(serverConfig.ip, serverConfig.port, ConnectCallBack, Socket);
        }

        public void Disconnect()
        {
            Socket.Dispose();
            Socket.Close();

            Socket = null;
            Stream = null;
            ReceivedData = null;
            ReceiveBuffer = null;
        }

        public void ReceiveCallBack(IAsyncResult iar)
        {
            try
            {
                var length = Stream.EndRead(iar);

                if (length <= 0)
                {
                    Server.Disconnect();
                    return;
                }

                var data = new byte[length];

                Array.Copy(ReceiveBuffer, data, length);

                ReceivedData.Reset(HandleData(data));
                Stream.BeginRead(ReceiveBuffer, 0, Const.DATA_BUFFER_SIZE, ReceiveCallBack, null);
            }
            catch (Exception e)
            {
                Log.Error($"Error receiving TCP data: {e}");

                Disconnect();
            }
        }

        public void SendData(Packet packet)
        {
            try
            {
                if (Socket != null)
                {
                    Stream.BeginWrite(packet.ToArray(), 0, packet.Length(), null, null);
                }
            }
            catch (Exception ex)
            {
                Log.Error($"Error sending data to server via TCP: {ex}");
            }
        }

        public bool HandleData(byte[] data)
        {
            var packetLength = 0;

            ReceivedData.SetBytes(data);

            if (ReceivedData.UnreadLength() >= 4)
            {
                packetLength = ReceivedData.ReadInt();

                if (packetLength <= 0)
                {
                    return true;
                }
            }

            while (packetLength > 0 && packetLength <= ReceivedData.UnreadLength())
            {
                var packetBytes = ReceivedData.ReadBytes(packetLength);

                Thread.RunOnMainThread(() =>
                {
                    using var packet = new Packet(packetBytes);
                    var id = packet.ReadInt();

                    Server.ClientHandlers[id](packet);
                });

                packetLength = 0;

                if (ReceivedData.UnreadLength() < 4)
                {
                    continue;
                }

                packetLength = ReceivedData.ReadInt();

                if (packetLength <= 0)
                {
                    return true;
                }
            }

            return packetLength <= 1;
        }

        #endregion

        #region Private

        private void ConnectCallBack(IAsyncResult iar)
        {
            Socket.EndConnect(iar);

            if (!Socket.Connected)
            {
                return;
            }

            Stream = Socket.GetStream();
            ReceivedData = new Packet();

            Stream.BeginRead(ReceiveBuffer, 0, Const.DATA_BUFFER_SIZE, ReceiveCallBack, null);
        }

        #endregion

        #region Protected

        #endregion
    }
}