﻿using System;
using System.Net;
using System.Net.Sockets;

namespace Client.Ex
{
    public static class IpEx
    {
        /// <summary>
        /// get your current ip address, default is ipv4 == AddressFamily.InterNetwork
        /// </summary>
        /// <param name="addressFamily"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static string GetLocalIPAddress(AddressFamily addressFamily = AddressFamily.InterNetwork)
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == addressFamily)
                {
                    return ip.ToString();
                }
            }

            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
    }
}