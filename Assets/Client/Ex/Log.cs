﻿#if UNITY_EDITOR || UNITY_WEBGL
using UnityEngine;

namespace Client.Ex
{
    public static class Log
    {
        public static void Debug(string msg, Object context = null)
        {
            Debug(msg, null, context);
        }

        public static void Error(string msg)
        {
            UnityEngine.Debug.LogError(msg);
        }

        public static void Warning(string msg)
        {
            UnityEngine.Debug.LogWarning(msg);
        }

        private static void DebugAssert(bool condition, string msg)
        {
            UnityEngine.Debug.Assert(condition, msg);
        }

        private static void Debug(string msg, Color? color, Object context)
        {
            UnityEngine.Debug.Log($"<color={color ?? Color.black}>{msg}</color>", context);
        }
    }
}
#endif