using UnityEngine;

namespace Tamarin.Common
{
    public abstract class Singleton<T> : MonoBehaviour where T : Component
    {
        public bool destroyable = false;

        private static T instance;
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<T>();
                    if (instance == null)
                    {
                        GameObject obj = new GameObject();
                        obj.name = typeof(T).Name;
                        obj.transform.SetParent(null);
                        instance = obj.AddComponent<T>();
                    }
                }
                return instance;
            }
        }

        public virtual void Awake()
        {
            if (instance == null)
            {
                instance = this as T;
                if (!destroyable)
                    DontDestroyOnLoad(gameObject);
            }
            else
                Destroy(gameObject);
        }
    }
}