using UnityEditor;
using UnityEngine;
using Tamarin.Common;

#if TT_FIREBASE && UNITY_EDITOR
using Newtonsoft.Json;
namespace Tamarin.FirebaseX
{
    [CustomEditor(typeof(FirebaseAPI))]
    [CanEditMultipleObjects]
    public class FirebaseAPIEditor : Editor
    {
        public override bool UseDefaultMargins() { return false; }
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorUtils.DrawBranding("API for Firebase", FirebaseSetup.discord);
            GUILayout.BeginVertical(new GUIStyle(EditorStyles.inspectorDefaultMargins));

            EditorGUILayout.Space(10);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("region"));
            EditorGUILayout.Space(10);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onInit"));
            EditorGUILayout.Space(10);
            GUILayout.EndVertical();

            serializedObject.ApplyModifiedProperties();

            GUILayout.Space(20);
            GUILayout.Label("Thank you for choosing API for Firebase. If you are happy please leave a review! If you have any trouble please contact us on our discord for help!", EditorStyles.wordWrappedMiniLabel);
        }
    }
}
#endif

#if !TT_FIREBASE && UNITY_EDITOR
namespace Tamarin.FirebaseX
{
    [CustomEditor(typeof(FirebaseAPI))]
    [CanEditMultipleObjects]
    public class FirebaseAPIEditor : Editor
    {
        public override bool UseDefaultMargins() { return false; }
        public override void OnInspectorGUI()
        {
            EditorUtils.DrawBranding("API for Firebase");
            GUILayout.BeginVertical(new GUIStyle(EditorStyles.inspectorDefaultMargins));

            GUILayout.Space(15);
            if (GUILayout.Button("Enable API"))
                FirebaseSetup.Init();
            GUILayout.Space(10);

            GUILayout.EndVertical();
        }
    }
}
#endif

