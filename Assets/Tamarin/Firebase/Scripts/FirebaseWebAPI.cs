/* TAMARIN FIREBASE API */
/* Copyright (c) 2021 TwistedTamarin ltd All Rights Reserved (https://twistedtamarin.com) */

using System;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using Tamarin.Common;
#if TT_FIREBASE && UNITY_WEBGL && !UNITY_EDITOR
using Newtonsoft.Json;

namespace Tamarin.FirebaseX
{
    public class FirebaseAPI : Singleton<FirebaseAPI>
    {
        private FirebaseWebHelper _helper;

        public bool ready;
        public bool isAuth => User.UserId != null;
        public FirebaseUser User => CurrentUser();
        public string pushToken;

        public UnityEvent onInit;

        public override void Awake()
        {
            base.Awake();
            name = "FirebaseWebAPI";
        }

        private void Start()
        {
            _helper = FirebaseWebHelper.Instance;
            Initialize();
        }

        public async void Initialize()
        {
            var config = JsonConvert.SerializeObject(_helper.config);
            string response = null;

            var cid = _helper.Add(res =>
            {
                Debug.Log("[TT] Firebase ready");
                ready = true;
                onInit?.Invoke();
            });

            InitializeWeb(config, cid);
            await Waiter.Until(() => response != null);
        }

        public void Dispose()
        {
        }

        public void SignOut()
        {
            SignOutWeb();
        }

        //- Firestore -//
        public async Task<T> QueryAs<T>(string path, string docId)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            QueryAsWeb(path, docId, "null", "false", cid);

            await Waiter.Until(() => response != null);
            return response == "NULL" ? default(T) : JsonConvert.DeserializeObject<T>(response);
        }

        public async Task<List<T>> QueryAs<T>(string path)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            QueryAsWeb(path, "null", "null", "true", cid);

            await Waiter.Until(() => response != null);
            return response == "NULL" ? default(List<T>) : JsonConvert.DeserializeObject<List<T>>(response);
        }

        public async Task<List<T>> QueryAs<T>(string path, List<FirebaseQuery> search, int limit = 10)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            QueryAsWeb(path, "null", JsonConvert.SerializeObject(search), "true", cid);

            await Waiter.Until(() => response != null);
            return response == "NULL" ? default(List<T>) : JsonConvert.DeserializeObject<List<T>>(response);
        }

        public void QueryListenAs<T>(string path, string docId, System.Action<T> callback)
        {
            var cid = _helper.Add(
                res =>
                {
                    callback.Invoke(res == "NULL" ? default(T) : JsonConvert.DeserializeObject<T>(res));
                }, false);
            QueryListenAsWeb(path, docId, "null", "false", cid);
        }

        public void QueryListenAs<T>(string path, List<FirebaseQuery> search, System.Action<List<T>> callback,
            int limit = 10)
        {
            var cid = _helper.Add(
                res =>
                {
                    callback.Invoke(res == "NULL" ? default(List<T>) : JsonConvert.DeserializeObject<List<T>>(res));
                }, false);
            QueryListenAsWeb(path, "null", JsonConvert.SerializeObject(search), "true", cid);
        }

        public async Task<bool> UpdateAsync(string path, string docId, object data)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            UpdateAsyncWeb(path, docId, JsonConvert.SerializeObject(data), cid);

            await Waiter.Until(() => response != null);
            return response == "NULL" ? false : JsonConvert.DeserializeObject<StatusModel>(response).status;
        }

        public async Task<string> SetAsync(string path, object data)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            SetAsyncWeb(path, "null", JsonConvert.SerializeObject(data), cid);

            await Waiter.Until(() => response != null);
            return response == "NULL" ? null : JsonConvert.DeserializeObject<StatusModel>(response).id;
        }

        public async Task<string> SetAsync(string path, string docId, object data)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            SetAsyncWeb(path, docId, JsonConvert.SerializeObject(data), cid);

            await Waiter.Until(() => response != null);
            return response == "NULL" ? null : JsonConvert.DeserializeObject<StatusModel>(response).id;
        }

        public async Task<bool> RemoveAsync(string path, string docId)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);

            RemoveAsyncWeb(path, docId, cid);
            await Waiter.Until(() => response != null);

            return response == "NULL" ? false : JsonConvert.DeserializeObject<StatusModel>(response).status;
        }

        public void Unsubscribe(string path, string docId)
        {
            UnsubscribeWeb(path, docId);
        }

        public void UnsubscribeAll()
        {
            UnsubscribeAllWeb();
        }

        //- Realtime Database (beta) -//
        public async Task<string> RealQueryAsync(string path)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            RealQueryAsyncWeb(path, "null", cid);

            await Waiter.Until(() => response != null);
            return response == "NULL" ? null : response;
        }

        public async Task<string> RealQueryAsync(string path, List<RealtimeQuery> query)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            RealQueryAsyncWeb(path, JsonConvert.SerializeObject(query), cid);

            await Waiter.Until(() => response != null);
            return response == "NULL" ? null : response;
        }

        public void RealQueryListenAsync(string path, Action<string> callback)
        {
            var cid = _helper.Add(res => { callback.Invoke(res == "NULL" ? null : res); }, false);
            RealQueryListenAsyncWeb(path, "null", cid);
        }

        public void RealQueryListenAsync(string path, Action<string> callback, List<RealtimeQuery> query)
        {
            var cid = _helper.Add(res => { callback.Invoke(res == "NULL" ? null : res); }, false);
            RealQueryListenAsyncWeb(path, JsonConvert.SerializeObject(query), cid);
        }

        public async Task<object> RealSetAsync(string path, object data)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);

            var json = JsonConvert.SerializeObject(data);
            RealSetAsyncWeb(path, json, cid);

            await Waiter.Until(() => response != null);
            return response == "NULL" ? null : response;
        }

        public async Task<object> RealUpdateAsync(string path, object data)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);

            var json = JsonConvert.SerializeObject(data);
            RealUpdateAsyncWeb(path, json, cid);

            await Waiter.Until(() => response != null);
            return response == "NULL" ? null : response;
        }

        //- Functions -//
        public async Task<T> HttpsCall<T>(string fn, object data)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            HttpsCallableWeb(fn, JsonConvert.SerializeObject(data), cid);

            await Waiter.Until(() => response != null);
            return response == "NULL" ? default(T) : JsonConvert.DeserializeObject<T>(response);
        }

        //- Auth -//
        public FirebaseUser CurrentUser()
        {
            var response = CurrentUserWeb();
            return response == "NULL" ? null : JsonConvert.DeserializeObject<FirebaseUser>(response);
        }

        public void AuthChanged(Action<FirebaseUser> callback)
        {
            var cid = _helper.Add(res =>
            {
                var response = res == "NULL" ? null : JsonConvert.DeserializeObject<FirebaseUser>(res);
                callback.Invoke(response);
            }, false);
            AuthRedirectWeb(cid);
        }

        public void AuthRedirect(Action<FirebaseUser> callback)
        {
            var cid = _helper.Add(res =>
            {
                var response = res == "NULL" ? null : JsonConvert.DeserializeObject<FirebaseUser>(res);
                callback.Invoke(response);
            }, false);
            AuthRedirectWeb(cid);
        }

        public async Task<FirebaseUser> SignInAnonymously()
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            SignInAnonymouslyWeb(cid);

            await Waiter.Until(() => response != null);
            return response == "NULL" ? null : JsonConvert.DeserializeObject<FirebaseUser>(response);
        }

        public async Task<FirebaseUser> SignInToken(string token)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            SignInTokenWeb(token, cid);

            await Waiter.Until(() => response != null);
            return response == "NULL" ? null : JsonConvert.DeserializeObject<FirebaseUser>(response);
        }

        public async Task<FirebaseUser> SignInEmail(string email, string password, bool link = false)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            SignInEmailWeb(email, password, link.ToString(), cid);

            await Waiter.Until(() => response != null);
            return response == "NULL" ? null : JsonConvert.DeserializeObject<FirebaseUser>(response);
        }

        public async Task<FirebaseUser> RegisterWithEmail(string email, string password)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            RegisterWithEmailWeb(email, password, cid);

            await Waiter.Until(() => response != null);
            return response == "NULL" ? null : JsonConvert.DeserializeObject<FirebaseUser>(response);
        }

        public async Task<FirebaseUser> SignInGoogle(string id, string accessToken, bool link = false,
            bool redirect = false)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            SignInGoogleWeb(cid, link.ToString(), redirect.ToString());

            await Waiter.Until(() => response != null);
            return response == "NULL" ? null : JsonConvert.DeserializeObject<FirebaseUser>(response);
        }

        public async Task<FirebaseUser> SignInFacebook(string accessToken, bool link = false, bool redirect = false)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            SignInFacebookWeb(cid, link.ToString(), redirect.ToString());

            await Waiter.Until(() => response != null);
            return response == "NULL" ? null : JsonConvert.DeserializeObject<FirebaseUser>(response);
        }

        public async Task<FirebaseUser> SignInApple(string token, string nonce, string code, bool link = false,
            bool redirect = false)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            SignInAppleWeb(cid, link.ToString(), redirect.ToString());

            await Waiter.Until(() => response != null);
            return response == "NULL" ? null : JsonConvert.DeserializeObject<FirebaseUser>(response);
        }

        public async Task<FirebaseUser> SignInMicrosoft(List<string> scopes, Dictionary<string, string> custom)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            SignInMicrosoftWeb(cid, JsonConvert.SerializeObject(custom), JsonConvert.SerializeObject(scopes));

            await Waiter.Until(() => response != null);
            return response == "NULL" ? null : JsonConvert.DeserializeObject<FirebaseUser>(response);
        }

        //- Remote Config -//
        public async Task<bool> RemoteSetup(Dictionary<string, object> config, ulong interval)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            RemoteSetupWeb(JsonConvert.SerializeObject(config), $"{interval}");

            await Waiter.Until(() => response != null);
            return true;
        }

        public async Task<string> RemoteGetValue(string key)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            RemoteGetValueWeb(key, cid);

            await Waiter.Until(() => response != null);
            return response;
        }

        public async Task<string> RemoteGetAll(string key)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            RemoteGetValueWeb(key, cid);

            await Waiter.Until(() => response != null);
            return response;
        }

        public async Task<bool> RemoteFetchActivate()
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            RemoteFetchActivateWeb(cid);

            await Waiter.Until(() => response != null);
            return true;
        }

        //- Messages
        public void MessageSetup(Action<string> callback)
        {
            var cid = _helper.Add(res =>
            {
                pushToken = res;
                callback.Invoke(pushToken);
            }, false);
            MessageSetupWeb(_helper.config.vapidKey, cid);
        }

        public void MessageOnReceived(Action<FirebaseMessage> callback)
        {
            var cid = _helper.Add(
                res =>
                {
                    callback.Invoke(res == "NULL"
                        ? default(FirebaseMessage)
                        : JsonConvert.DeserializeObject<FirebaseMessage>(res));
                }, false);
            MessageOnReceivedWeb(cid);
        }

        public void MessageSubscribe(string topic)
        {
            Debug.Log("There is no topic subscription api on webGL! check the docs for a workaround!");
        }

        public void MessageUnsubscribe(string topic)
        {
            Debug.Log("There is no topic subscription api on webGL! check the docs for a workaround!");
        }

        //- Storage -//
        public async Task<bool> StorageUpload(string path, byte[] data, string type)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            StorageUploadWeb("null", path, data, type, $"{data.Length}", cid);

            await Waiter.Until(() => response != null);
            return true;
        }

        public async Task<bool> StorageUpload(string bucket, string path, byte[] data, string type)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            StorageUploadWeb(bucket, path, data, type, $"{data.Length}", cid);

            await Waiter.Until(() => response != null);
            return true;
        }

        public async Task<string> StorageDownloadUrl(string path)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            StorageDownloadUrlWeb("null", path, cid);

            await Waiter.Until(() => response != null);
            return response;
        }

        public async Task<string> StorageDownloadUrl(string bucket, string path)
        {
            string response = null;
            var cid = _helper.Add(res => response = res);
            StorageDownloadUrlWeb(bucket, path, cid);

            await Waiter.Until(() => response != null);
            return response;
        }

        //- Analytics
        public void AnalyticsSetup(bool status)
        {
            AnalyticsSetupWeb("" + status, "null");
        }

        public void AnalyticsSetup(bool status, TimeSpan timeout)
        {
            AnalyticsSetupWeb("" + status, "" + timeout);
        }

        public void AnalyticsSetUserId(string uid)
        {
            AnalyticsSetUserIdWeb(uid);
        }

        public void AnalyticsSetUserProperty(string prop, string value)
        {
            AnalyticsSetUserPropertyWeb(prop, value);
        }

        public void AnalyticsSetCurrentScreen(string name, string cat)
        {
            AnalyticsSetCurrentScreenWeb(name, cat);
        }

        public void AnalyticsLogEvent(string name)
        {
            AnalyticsLogEventWeb(name, "null", "null");
        }

        public void AnalyticsLogEvent(string name, string param, string value)
        {
            AnalyticsLogEventWeb(name, param, value);
        }

        public void AnalyticsLogEvent(string name, string param, int value)
        {
            AnalyticsLogEventWeb(name, param, "" + value);
        }

        public void AnalyticsLogEvent(string name, string param, double value)
        {
            AnalyticsLogEventWeb(name, param, "" + value);
        }

        public void AnalyticsLogEvent(string name, string param, float value)
        {
            AnalyticsLogEventWeb(name, param, "" + value);
        }

        public void AnalyticsLogEvent(string name, string param, long value)
        {
            AnalyticsLogEventWeb(name, param, "" + value);
        }

        //- Hooks -//
        [DllImport("__Internal")]
        private static extern string InitializeWeb(string config, string cid);

        [DllImport("__Internal")]
        private static extern string QueryAsWeb(string path, string docId, string data, string flag, string cid);

        [DllImport("__Internal")]
        private static extern string QueryListenAsWeb(string path, string docId, string data, string flag, string cid);

        [DllImport("__Internal")]
        private static extern string SetAsyncWeb(string path, string docId, string data, string cid);

        [DllImport("__Internal")]
        private static extern string UpdateAsyncWeb(string path, string docId, string data, string cid);

        [DllImport("__Internal")]
        private static extern string RemoveAsyncWeb(string path, string docId, string cid);

        [DllImport("__Internal")]
        private static extern string UnsubscribeWeb(string path, string docId);

        [DllImport("__Internal")]
        private static extern string UnsubscribeAllWeb();

        [DllImport("__Internal")]
        private static extern string RealQueryAsyncWeb(string path, string query, string cid);

        [DllImport("__Internal")]
        private static extern string RealQueryListenAsyncWeb(string path, string query, string cid);

        [DllImport("__Internal")]
        private static extern string RealSetAsyncWeb(string path, string data, string cid);

        [DllImport("__Internal")]
        private static extern string RealUpdateAsyncWeb(string path, string data, string cid);

        [DllImport("__Internal")]
        private static extern string HttpsCallableWeb(string path, string data, string cid);

        [DllImport("__Internal")]
        private static extern string CurrentUserWeb();

        [DllImport("__Internal")]
        private static extern string AuthChangedWeb(string cid);

        [DllImport("__Internal")]
        private static extern string AuthRedirectWeb(string cid);

        [DllImport("__Internal")]
        private static extern string SignInAnonymouslyWeb(string cid);

        [DllImport("__Internal")]
        private static extern string SignInTokenWeb(string token, string cid);

        [DllImport("__Internal")]
        private static extern string SignInEmailWeb(string email, string password, string link, string cid);

        [DllImport("__Internal")]
        private static extern string RegisterWithEmailWeb(string email, string password, string cid);

        [DllImport("__Internal")]
        private static extern string SignInGoogleWeb(string cid, string link, string redirect);

        [DllImport("__Internal")]
        private static extern string SignInFacebookWeb(string cid, string link, string redirect);

        [DllImport("__Internal")]
        private static extern string SignInAppleWeb(string cid, string link, string redirect);

        [DllImport("__Internal")]
        private static extern string SignInMicrosoftWeb(string cid, string custom, string scopes);

        [DllImport("__Internal")]
        private static extern string SignOutWeb();

        [DllImport("__Internal")]
        private static extern void RemoteSetupWeb(string config, string interval);

        [DllImport("__Internal")]
        private static extern void RemoteGetValueWeb(string key, string cid);

        [DllImport("__Internal")]
        private static extern void RemoteGetAllWeb(string cid);

        [DllImport("__Internal")]
        private static extern void RemoteFetchActivateWeb(string cid);

        [DllImport("__Internal")]
        private static extern void MessageSetupWeb(string vapidKey, string cid);

        [DllImport("__Internal")]
        private static extern void MessageOnReceivedWeb(string cid);

        [DllImport("__Internal")]
        private static extern void StorageUploadWeb(string bucket, string path, byte[] data, string type, string size,
            string cid);

        [DllImport("__Internal")]
        private static extern void StorageDownloadUrlWeb(string bucket, string path, string cid);

        [DllImport("__Internal")]
        private static extern void AnalyticsSetupWeb(string status, string timestamp);

        [DllImport("__Internal")]
        private static extern void AnalyticsSetUserIdWeb(string id);

        [DllImport("__Internal")]
        private static extern void AnalyticsSetUserPropertyWeb(string prop, string value);

        [DllImport("__Internal")]
        private static extern void AnalyticsSetCurrentScreenWeb(string name, string cat);

        [DllImport("__Internal")]
        private static extern void AnalyticsLogEventWeb(string name, string param, string value);
    }
}

#endif

#if !TT_FIREBASE && UNITY_WEBGL && !UNITY_EDITOR
namespace Tamarin.FirebaseX
{
    public class FirebaseAPI : Singleton<FirebaseAPI> { }
}
#endif