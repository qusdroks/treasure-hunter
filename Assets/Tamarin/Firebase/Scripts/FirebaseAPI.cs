/* TAMARIN FIREBASE API */
/* Copyright (c) 2021 TwistedTamarin ltd All Rights Reserved (https://twistedtamarin.com) */

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using GameAssets.Scripts.Ex;
using UnityEngine;
using UnityEngine.Events;
using Tamarin.Common;

#if TT_FIREBASE && (UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR || UNITY_STANDALONE)
using System.Collections;
using Newtonsoft.Json;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Functions;
using Firebase.Firestore;
using Firebase.Extensions;
using Firebase.Analytics;
using Firebase.RemoteConfig;
using Firebase.Messaging;
using Firebase.Storage;

namespace Tamarin.FirebaseX
{
    public class FirebaseAPI : Singleton<FirebaseAPI>
    {
        private FirebaseApp _app;
        private FirebaseAuth _authref;
        private FirebaseFirestore _firestore;
        private FirebaseFunctions _functions;
        private FirebaseDatabase _database;
        private FirebaseRemoteConfig _remote;
        private FirebaseStorage _storage;

        public bool ready;

        public bool IsAuth => _authref.CurrentUser.UserId != null;

        public FirebaseUser user;

        public FirebaseUser User
        {
            get => user ?? new FirebaseUser(_authref.CurrentUser);
            set => user = value;
        }

        public string pushToken;

        public string region = "europe-west";
        public UnityEvent onInit;

        private List<Subscriber> _subscribers = new List<Subscriber>();

        public override void Awake()
        {
            base.Awake();
            name = "FirebaseAPI";
        }

        private void Start()
        {
            Initialize();
        }

        public void Initialize()
        {
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread((task) =>
            {
                var status = task.Result;

                if (status != DependencyStatus.Available)
                {
                    return;
                }

                _app = FirebaseApp.DefaultInstance;
                _authref = FirebaseAuth.GetAuth(_app);
                _firestore = FirebaseFirestore.DefaultInstance;

                var _ = new FirebaseFirestoreSettings
                {
                    PersistenceEnabled = false
                };

                _functions = FirebaseFunctions.GetInstance(region);
                _database = FirebaseDatabase.DefaultInstance;
                _remote = FirebaseRemoteConfig.DefaultInstance;
                _storage = FirebaseStorage.DefaultInstance;

                Debug.Log("[TT] Firebase ready");
                ready = true;
                onInit?.Invoke();
            });
        }

        public void Dispose()
        {
        }

        //- Firestore -//
        public async Task<T> QueryAs<T>(string path, string docId)
        {
            var docRef = _firestore.Collection(path).Document(docId);
            return await QueryAs<T>(docRef);
        }

        public async Task<List<T>> QueryAs<T>(string path)
        {
            var docRef = _firestore.Collection(path);
            return await QueryAs<T>(docRef);
        }

        public async Task<List<T>> QueryAs<T>(string path, List<FirebaseQuery> search, int limit = 10)
        {
            var query = BuildQuery(path, search);
            query.Limit(limit);
            return await QueryAs<T>(query);
        }

        public async Task<T> QueryAs<T>(DocumentReference docRef)
        {
            var document = await docRef.GetSnapshotAsync();

            if (!document.Exists)
            {
                return default;
            }

            var doc = document.ToDictionary();
            doc["docId"] = document.Id;

            var result = JsonConvert.SerializeObject(doc);
            return JsonConvert.DeserializeObject<T>(result);
        }

        public async Task<List<T>> QueryAs<T>(Firebase.Firestore.Query queryRef)
        {
            var response = await queryRef.GetSnapshotAsync();

            if (response.Count <= 0)
            {
                return new List<T>() {default};
            }

            var results = new List<T>();

            foreach (var document in response.Documents)
            {
                var doc = document.ToDictionary();
                doc["docId"] = document.Id;

                var result = JsonConvert.SerializeObject(doc);
                results.Add(JsonConvert.DeserializeObject<T>(result));
            }

            return results;
        }

        public void QueryListenAs<T>(string path, string docId, Action<T> callback)
        {
            var docRef = _firestore.Collection(path).Document(docId);

            var sub = docRef.Listen(document =>
            {
                var doc = document.ToDictionary();
                doc["docId"] = document.Id;

                var result = JsonConvert.SerializeObject(doc);
                callback.Invoke(JsonConvert.DeserializeObject<T>(result));
            });

            _subscribers.Add(new Subscriber {path = path, docId = docId, sub = sub});
        }

        public void QueryListenAs<T>(string path, List<FirebaseQuery> search, Action<List<T>> callback,
            int limit = 10)
        {
            var query = BuildQuery(path, search);
            query.Limit(limit);

            var sub = query.Listen((snapshot) =>
            {
                var results = new List<T>();

                foreach (var document in snapshot.Documents)
                {
                    var doc = document.ToDictionary();
                    doc["docId"] = document.Id;

                    var result = JsonConvert.SerializeObject(doc);
                    results.Add(JsonConvert.DeserializeObject<T>(result));
                }

                callback.Invoke(results);
            });

            _subscribers.Add(new Subscriber {path = path, docId = null, sub = sub});
        }

        public Firebase.Firestore.Query BuildQuery(string path, List<FirebaseQuery> search)
        {
            Firebase.Firestore.Query query = _firestore.Collection(path);

            foreach (var item in search)
            {
                switch (item.op)
                {
                    case "==":
                        query = query.WhereEqualTo(item.prop, item.value);
                        break;

                    case "<=":
                        query = query.WhereLessThanOrEqualTo(item.prop, item.value);
                        break;

                    case ">=":
                        query = query.WhereGreaterThanOrEqualTo(item.prop, item.value);
                        break;

                    case "<":
                        query = query.WhereLessThan(item.prop, item.value);
                        break;

                    case ">":
                        query = query.WhereGreaterThan(item.prop, item.value);
                        break;

                    case "in":
                        query = query.WhereIn(item.prop, item.list);
                        break;

                    case "array-contains":
                        query = query.WhereArrayContains(item.prop, item.value);
                        break;

                    case "array-contains-any":
                        query = query.WhereArrayContainsAny(item.prop, item.list);
                        break;
                }
            }

            return query;
        }

        public async Task<bool> UpdateAsync(string path, string docId, Dictionary<string, object> data)
        {
            var docRef = _firestore.Collection(path).Document(docId);

            await docRef.UpdateAsync(data).ContinueWithOnMainThread((task) =>
            {
                if (task.Exception != null)
                {
                    Debug.Log($"[TT] Firebase update error: {docId} - {task.Exception}");
                }
            });

            return true;
        }

        public async Task<string> SetAsync(string path, string docId, object data)
        {
            var docRef = _firestore.Collection(path).Document(docId);

            await docRef.SetAsync(data, SetOptions.MergeAll).ContinueWithOnMainThread(task =>
            {
                if (task.Exception != null)
                {
                    Debug.Log($"[TT] Firebase set error: {path} - {task.Exception}");
                }
            });

            return docRef.Id;
        }

        public async Task<string> SetAsync(string path, object data)
        {
            var docRef = _firestore.Collection(path).Document();

            await docRef.SetAsync(data, SetOptions.MergeAll).ContinueWithOnMainThread(task =>
            {
                if (task.Exception != null)
                {
                    Debug.Log($"[TT] Firebase set error: {path} - {task.Exception}");
                }
            });

            return docRef.Id;
        }

        public async Task<bool> RemoveAsync(string path, string docId)
        {
            var docRef = _firestore.Collection(path).Document(docId);

            await docRef.DeleteAsync().ContinueWithOnMainThread(task =>
            {
                if (task.Exception != null)
                {
                    Debug.Log($"[TT] Firebase remove error: {path} - {task.Exception}");
                }
            });

            return true;
        }

        public void Unsubscribe(string path, string docId)
        {
            var data = docId == null
                ? _subscribers.FirstOrDefault(x => x.path == path)
                : _subscribers.FirstOrDefault(x => x.path == path && x.docId == docId);
            data?.sub.Stop();
        }

        public void UnsubscribeAll()
        {
            foreach (var data in _subscribers)
            {
                data.sub.Stop();
            }
        }

        //- Realtime Database -//
        public async Task<string> RealQueryAsync(string path)
        {
            var result = await _database.GetReference(path).GetValueAsync();
            return result.Exists ? result.GetRawJsonValue() : $"ERROR: Value doesn't exist at:{path}";
        }

        public async Task<string> RealQueryAsync(string path, List<RealtimeQuery> query)
        {
            var db = RealBuildQuery(path, query);
            var result = await db.GetValueAsync();
            return result.Exists ? result.GetRawJsonValue() : $"ERROR: Value doesn't exist at:{path}";
        }

        public void RealQueryListenAsync(string path, Action<string> callback)
        {
            var db = _database.GetReference(path);
            db.ValueChanged += (sender, args) =>
            {
                callback.Invoke(args.Snapshot.Exists ? args.Snapshot.GetRawJsonValue() : "Error");
            };
        }

        public void RealQueryListenAsync(string path, Action<string> callback, List<RealtimeQuery> query)
        {
            var db = RealBuildQuery(path, query);
            db.ValueChanged += (sender, args) =>
            {
                callback.Invoke(args.Snapshot.Exists
                    ? args.Snapshot.GetRawJsonValue()
                    : args.DatabaseError.Message);
            };
        }

        public async void RealSetAsync(string path, object data)
        {
            var db = _database.RootReference;
            string json;

            if (data.IsList() && ((IList) data).Count < 1)
            {
                json = ((IList) data).Count < 1 ? null : JsonConvert.SerializeObject(data);
            }
            else
            {
                json = JsonConvert.SerializeObject(data);
            }

            await db.Child(path).SetRawJsonValueAsync(json);
        }

        public async void RealUpdateAsync(string path, object data)
        {
            var db = _database.RootReference;
            var json = JsonConvert.SerializeObject(data);
            await db.Child(path).SetRawJsonValueAsync(json);
        }

        public Firebase.Database.Query RealBuildQuery(string path, List<RealtimeQuery> search)
        {
            Firebase.Database.Query query = _database.GetReference(path);
            foreach (var item in search)
            {
                switch (item.op)
                {
                    case "orderByChild":
                        query = query.OrderByChild((string) item.value);
                        break;
                    case "orderByKey":
                        query = query.OrderByKey();
                        break;
                    case "orderByValue":
                        query = query.OrderByValue();
                        break;
                    case "limitToFirst":
                        query = query.LimitToFirst((int) item.value);
                        break;
                    case "limitToLast":
                        query = query.LimitToLast((int) item.value);
                        break;
                    case "startAt":
                        query = query.StartAt((string) item.value, item.key);
                        break;
                    case "endAt":
                        query = query.EndAt((string) item.value, item.key);
                        break;
                    case "equalTo":
                        query = query.EqualTo((string) item.value, item.key);
                        break;
                    default: break;
                }
            }

            return query;
        }

        //- Functions -//
        public async Task<T> HttpsCall<T>(string fn, object data)
        {
            var callref = _functions.GetHttpsCallable(fn);
            var response = await callref.CallAsync(data);
            var result = JsonConvert.SerializeObject(response.Data);
            return JsonConvert.DeserializeObject<T>(result);
        }

        //- Auth -//
        public void AuthChanged(Action<FirebaseUser> callback)
        {
            _authref.StateChanged += (sender, eventArgs) => { callback.Invoke(User); };
        }

        public void AuthRedirect(Action<FirebaseUser> callback)
        {
            Debug.Log("Redirects only supported by web!");
        }

        public async Task<FirebaseUser> SignInAnonymously()
        {
            User = await _authref.SignInAnonymouslyAsync().ContinueWithOnMainThread(ResolveUser);
            return User;
        }

        public async Task<FirebaseUser> SignInToken(string token)
        {
            User = await _authref.SignInWithCustomTokenAsync(token).ContinueWithOnMainThread(ResolveUser);
            return User;
        }

        public async Task<FirebaseUser> SignInEmail(string email, string password, bool link = false)
        {
            if (link && User.status)
            {
                var credential = EmailAuthProvider.GetCredential(email, password);
                User = await _authref.CurrentUser.LinkWithCredentialAsync(credential)
                    .ContinueWithOnMainThread(ResolveUser);
                return User;
            }

            User = await _authref.SignInWithEmailAndPasswordAsync(email, password).ContinueWithOnMainThread(ResolveUser);
            return User;
        }

        public async Task<FirebaseUser> RegisterWithEmail(string email, string password)
        {
            User = await _authref.CreateUserWithEmailAndPasswordAsync(email, password)
                .ContinueWithOnMainThread(ResolveUser);
            return User;
        }

        public async Task<FirebaseUser> SignInGoogle(string id, string accessToken, bool link = false,
            bool redirect = false)
        {
            var credential = GoogleAuthProvider.GetCredential(id, null);
            return await SignInCredential(credential, link);
        }

        public async Task<FirebaseUser> SignInFacebook(string accessToken, bool link = false, bool redirect = false)
        {
            var credential = FacebookAuthProvider.GetCredential(accessToken);
            return await SignInCredential(credential, link);
        }

        public async Task<FirebaseUser> SignInApple(string token, string nonce, string code, bool link = false,
            bool redirect = false)
        {
            var credential = OAuthProvider.GetCredential("apple.com", token, nonce, code);
            return await SignInCredential(credential, link);
        }

        public async Task<FirebaseUser> SignInMicrosoft(List<string> scopes, Dictionary<string, string> custom)
        {
            var data = new FederatedOAuthProviderData();
            data.ProviderId = MicrosoftAuthProvider.ProviderId;
            data.Scopes = scopes;
            foreach (var c in custom)
            {
                data.CustomParameters.Add(c.Key, c.Value);
            }

            var provider = new FederatedOAuthProvider();
            provider.SetProviderData(data);
            return await SignInProvider(provider);
        }

        public async Task<FirebaseUser> SignInCredential(Credential credential, bool link = false)
        {
            if (link && User.status)
            {
                User = await _authref.CurrentUser.LinkWithCredentialAsync(credential)
                    .ContinueWithOnMainThread(ResolveUser);
                return User;
            }

            User = await _authref.SignInWithCredentialAsync(credential).ContinueWithOnMainThread(ResolveUser);
            return User;
        }

        public async Task<FirebaseUser> SignInProvider(FederatedOAuthProvider provider)
        {
            User = await _authref.SignInWithProviderAsync(provider).ContinueWithOnMainThread(ResolveUser);
            return User;
        }

        public FirebaseUser ResolveUser(Task<Firebase.Auth.FirebaseUser> task)
        {
            if ((!task.IsFaulted && !task.IsCanceled) || task.Exception == null)
            {
                return new FirebaseUser(task.Result);
            }

            var e = (FirebaseException) task.Exception.GetBaseException();
            return new FirebaseUser(e.ErrorCode.ToString(), e.Message);
        }

        public FirebaseUser ResolveUser(Task<SignInResult> task)
        {
            if ((task.IsFaulted || task.IsCanceled) && task.Exception != null)
            {
                var e = ((FirebaseException) task.Exception.GetBaseException());
                return new FirebaseUser(e.ErrorCode.ToString(), e.Message);
            }

            return new FirebaseUser(task.Result.User);
        }

        public void SignOut()
        {
            _authref.SignOut();
        }

        //- Remote Config -//
        public async Task<bool> RemoteSetup(Dictionary<string, object> config, ulong interval)
        {
            await _remote.SetDefaultsAsync(config);
            await _remote.SetConfigSettingsAsync(new ConfigSettings {MinimumFetchInternalInMilliseconds = interval});
            return true;
        }

        public async Task<string> RemoteGetValue(string key)
        {
            var tsc = new TaskCompletionSource<string>();
            tsc.SetResult(_remote.GetValue(key).StringValue);
            return await tsc.Task;
        }

        public async Task<string> RemoteGetAll(string key)
        {
            var tsc = new TaskCompletionSource<string>();
            tsc.SetResult(JsonConvert.SerializeObject(_remote.AllValues));
            return await tsc.Task;
        }

        public async Task<bool> RemoteFetchActivate()
        {
            return await _remote.FetchAndActivateAsync();
        }

        //- Messages
        public void MessageSetup(Action<string> callback)
        {
            FirebaseMessaging.TokenReceived += (sender, token) =>
            {
                if (token != null)
                {
                    callback.Invoke(token.Token);
                }
            };
        }

        public void MessageOnReceived(Action<FirebaseMessage> callback)
        {
            FirebaseMessaging.MessageReceived += (sender, data) =>
            {
                if (data == null || data.Message == null || data.Message.Notification == null)
                {
                    callback.Invoke(null);
                    return;
                }

                var msg = new FirebaseMessage(data);
                callback?.Invoke(msg);
            };
        }

        public void MessageSubscribe(string topic)
        {
            FirebaseMessaging.SubscribeAsync(topic);
        }

        public void MessageUnsubscribe(string topic)
        {
            FirebaseMessaging.UnsubscribeAsync(topic);
        }

        //- Storage -//
        public async Task<bool> StorageUpload(string path, byte[] data, string type)
        {
            var meta = new MetadataChange
            {
                ContentType = type
            };

            var pref = _storage.GetReference(path);
            var res = await pref.PutBytesAsync(data, meta);
            return (res.Md5Hash != null) ? true : false;
        }

        public async Task<bool> StorageUpload(string bucket, string path, byte[] data, string type)
        {
            var meta = new MetadataChange
            {
                ContentType = type
            };

            var bref = FirebaseStorage.GetInstance(bucket);
            var pref = bref.GetReference(path);
            var res = await pref.PutBytesAsync(data, meta);
            return (res.Md5Hash != null) ? true : false;
        }

        public async Task<string> StorageDownloadUrl(string path)
        {
            var pref = _storage.GetReference(path);
            var res = await pref.GetDownloadUrlAsync();
            return (res != null) ? res.ToString() : null;
        }

        public async Task<string> StorageDownloadUrl(string bucket, string path)
        {
            var bref = FirebaseStorage.GetInstance(bucket);
            var pref = bref.GetReference(path);
            var res = await pref.GetDownloadUrlAsync();
            return (res != null) ? res.ToString() : null;
        }

        //- Analytics
        public void AnalyticsSetup(bool status)
        {
            FirebaseAnalytics.SetAnalyticsCollectionEnabled(status);
        }

        public void AnalyticsSetup(bool status, TimeSpan timeout)
        {
            FirebaseAnalytics.SetAnalyticsCollectionEnabled(status);
            FirebaseAnalytics.SetSessionTimeoutDuration(timeout);
        }

        public void AnalyticsSetUserId(string uid)
        {
            FirebaseAnalytics.SetUserId(uid);
        }

        public void AnalyticsSetUserProperty(string prop, string value)
        {
            FirebaseAnalytics.SetUserProperty(prop, value);
        }

        public void AnalyticsSetCurrentScreen(string name, string cat)
        {
            FirebaseAnalytics.SetCurrentScreen(name, cat);
        }

        public void AnalyticsLogEvent(string name)
        {
            FirebaseAnalytics.LogEvent(name);
        }

        public void AnalyticsLogEvent(string name, string param, string value)
        {
            FirebaseAnalytics.LogEvent(name, param, value);
        }

        public void AnalyticsLogEvent(string name, string param, int value)
        {
            FirebaseAnalytics.LogEvent(name, param, value);
        }

        public void AnalyticsLogEvent(string name, string param, double value)
        {
            FirebaseAnalytics.LogEvent(name, param, value);
        }

        public void AnalyticsLogEvent(string name, string param, float value)
        {
            FirebaseAnalytics.LogEvent(name, param, value);
        }

        public void AnalyticsLogEvent(string name, string param, long value)
        {
            FirebaseAnalytics.LogEvent(name, param, value);
        }
    }

    public class Subscriber
    {
        public string path;
        public string docId;
        public ListenerRegistration sub;
    }
}
#endif

#if !TT_FIREBASE && (UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR)
namespace Tamarin.FirebaseX
{
    public class FirebaseAPI : Singleton<FirebaseAPI> { }
}
#endif