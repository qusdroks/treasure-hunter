/* TAMARIN FIREBASE API */
/* Copyright (c) 2021 TwistedTamarin ltd All Rights Reserved (https://twistedtamarin.com) */

using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Tamarin.Common;
#if TT_FIREBASE
using Newtonsoft.Json;

namespace Tamarin.FirebaseX
{
    public class FirebaseWebHelper : Singleton<FirebaseWebHelper>
    {
        public FirebaseConfig config;
        public List<FirebaseCallback> callbacks = new List<FirebaseCallback>();

        override public void Awake()
        {
            base.Awake();
            name = "FirebaseWebHelper";
        }

        public void CallbackHandler(string result)
        {
            var response = JsonConvert.DeserializeObject<FirebaseCallbackResponse>(result);
            var action = callbacks.FirstOrDefault(x => x.id == response.id);
            if (action != null)
            {
                action.callback.Invoke((!response.status) ? "NULL" : response.data);
                if (action.once)
                    callbacks.Remove(action);
            }
        }

        public string Add(Action<string> callback)
        {
            return Add(callback, true);
        }

        public string Add(Action<string> callback, bool once)
        {
            var id = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Replace('+', '@').Replace('/', '$')
                .Substring(4, 16);
            callbacks.Add(new FirebaseCallback {id = id, callback = callback, once = once});
            return id;
        }
    }

    public class FirebaseCallback
    {
        public string id;
        public bool once;
        public Action<string> callback;

        public FirebaseCallback()
        {
        }
    }

    public class FirebaseCallbackResponse
    {
        public string id;
        public bool status;
        public string data;

        public FirebaseCallbackResponse()
        {
        }
    }
}
#endif

#if !TT_FIREBASE
namespace Tamarin.FirebaseX
{
    public class FirebaseWebHelper : Singleton<FirebaseWebHelper> { }
}
#endif