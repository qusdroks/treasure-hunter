﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.Base;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.Manager.Audio
{
    public partial class AudioManager : BaseSingleton<AudioManager>
    {
        [SerializeField] private List<AudioClip> audioClips = new List<AudioClip>();

        [Header("[AUDIO SOURCE]")] [SerializeField]
        private AudioSource audioSound;

        [SerializeField] private AudioSource audioMusic;

        [Header("[SOUND]")] [SerializeField] private Button btnSound;

        [ShowIf(nameof(soundMini))] [SerializeField]
        private Button btnSoundMini;

        [SerializeField] private Image imgSound;

        [ShowIf(nameof(soundMini))] [SerializeField]
        private Image imgSoundMini;

        [ShowIf(nameof(useSlider))] [SerializeField]
        private Slider sliderSound;

        [SerializeField] private Sprite icSoundOn;
        [SerializeField] private Sprite icSoundOff;

        [Header("[MUSIC]")] [HideIf(nameof(soundAsMusic))] [SerializeField]
        private Button btnMusic;

        [HideIf(nameof(soundAsMusic))] [SerializeField]
        private Image imgMusic;

        [ShowIf("@!this.soundAsMusic && this.useSlider")] [SerializeField]
        private Slider sliderMusic;

        [HideIf(nameof(soundAsMusic))] [SerializeField]
        private Sprite icMusicOn;

        [HideIf(nameof(soundAsMusic))] [SerializeField]
        private Sprite icMusicOff;

        [Header("[VIBRATION]")] [SerializeField]
        private Button btnVibration;

        [SerializeField] private Image imgVibration;

        [SerializeField] private Sprite icVibrationOn;
        [SerializeField] private Sprite icVibrationOff;

        [Header("[BOOL]")] [SerializeField] private bool soundMini;
        [SerializeField] private bool soundAsMusic;
        [SerializeField] private bool useSlider;

        private Dictionary<string, AudioClip> _audioClipDictionaries = new Dictionary<string, AudioClip>();

        private int _loop;

        private void OnChangeSound()
        {
            audioSound.volume = UserDataManager.Instance.userDataSave.soundVolume;

            if (audioSound.volume <= 0f)
            {
                audioSound.Stop();

                if (soundAsMusic)
                {
                    audioMusic.Stop();
                    return;
                }
            }

            if (soundAsMusic)
            {
                OnChangeMusic();
            }
        }

        private void OnChangeMusic()
        {
            audioMusic.volume = soundAsMusic
                ? UserDataManager.Instance.userDataSave.soundVolume
                : UserDataManager.Instance.userDataSave.musicVolume;

            if (audioMusic.volume >= 1f)
            {
                PlayMusic("map-shop2");
            }
            else
            {
                audioMusic.Stop();
            }
        }

        private void ChangeImgSound()
        {
            imgSound.sprite = UserDataManager.Instance.HasSound ? icSoundOn : icSoundOff;

            OnChangeSound();
        }

        private void ChangeImgSoundMini()
        {
            if (!soundMini || imgSoundMini == null)
            {
                return;
            }

            imgSoundMini.sprite = UserDataManager.Instance.HasSound ? icSoundOn : icSoundOff;

            OnChangeSound();
        }

        private void ChangeImgMusic()
        {
            imgMusic.sprite = UserDataManager.Instance.HasMusic ? icMusicOn : icMusicOff;

            OnChangeMusic();
        }

        private void ChangeImgVibration()
        {
            imgVibration.sprite = UserDataManager.Instance.userDataSave.vibration
                ? icVibrationOn
                : icVibrationOff;
        }

        private AudioClip GetClip(string soundName)
        {
            return _audioClipDictionaries.TryGetValue(soundName, out var x) ? x : null;
        }

        private async UniTask Loop(AudioClip clip, int loop)
        {
            while (true)
            {
                _loop += 1;

                audioSound.Play();

                await UniTask.Delay(TimeSpan.FromSeconds(clip.length / 2));

                if (_loop < loop)
                {
                    continue;
                }

                break;
            }
        }

        private void UpdateImageSound()
        {
            ChangeImgSoundMini();
            ChangeImgSound();
        }
    }
}