using GameAssets.Scripts.Base;
using GameAssets.Scripts.UI.UILoading;
using UnityEngine;

namespace GameAssets.Scripts.Manager
{
    public class LoadingManager : BaseSingleton<LoadingManager>
    {
        [SerializeField] private UILoading uiLoading;
    }
}