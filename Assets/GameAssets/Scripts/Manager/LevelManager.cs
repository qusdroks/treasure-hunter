﻿using GameAssets.Scripts.Base;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.Manager.Game;
using UnityEngine;

namespace GameAssets.Scripts.Manager
{
    public class LevelManager : BaseSingleton<LevelManager>
    {
        [SerializeField] private Transform parent;

        [SerializeField] private bool isCreate = true;

        protected override void Awake()
        {
            base.Awake();
            LoadLevel();
        }

        private void LoadLevel()
        {
            if (!isCreate)
            {
                return;
            }

            var clip = GameManager.SelectedLevel.clip;
            var level = SpawnerEx.CreateSpawner(Vector3.zero, parent, GameManager.SelectedLevel.prefab);
            
            level.transform.localPosition = Vector3.zero;

            if (clip != null)
            {
                AudioManager.Instance.PlayMusic(clip.name);
            }
        }
    }
}