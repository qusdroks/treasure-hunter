﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Quest;
using GameAssets.Scripts.Model;
using GameAssets.Scripts.Model.Item;
using GameAssets.Scripts.UI.UIMaintenance;
using Newtonsoft.Json;
using Tamarin.FirebaseX;
using UnityEngine;
using UnityEngine.Networking;

namespace GameAssets.Scripts.Manager.Game
{
    public partial class GameManager
    {
        private static List<QuestData> Template { get; set; } = new List<QuestData>();

        private static Dictionary<string, Item> ResourceItemsDictionaries { get; set; } =
            new Dictionary<string, Item>();

        public static Dictionary<string, Map> ResourceMapsDictionaries { get; set; } =
            new Dictionary<string, Map>();

        public static List<Item> ShopItems { get; } = new List<Item>();

        public static User User { get; private set; }
        public static Config Config { get; private set; }
        public static Map SelectedMap { get; private set; }
        public static MapData SelectedMapData { get; private set; }
        public static Level SelectedLevel { get; private set; }
        public static Camera Camera { get; private set; }

        private static int SelectedLevelIndex { get; set; }
        private static int SelectedMapDataIndex { get; set; }

        public static async void AddAllAsync()
        {
            User.Items.Clear();
            User.Maps.Clear();

            foreach (var x in ResourceItemsDictionaries)
            {
                User.AddItem(x.Value, 1);
            }

            foreach (var x in ResourceMapsDictionaries)
            {
                foreach (var y in x.Value.levels)
                {
                    y.completed = true;
                }

                User.Maps.Add(MapData.Create(x.Value));
            }

            await UpdateUser(() => { }, () => { });
        }

        public static async UniTask<WorldTime> GetWorldTime()
        {
            var request = UnityWebRequest.Get(Const.BASE_URL_WORLD_TIME);

            Debug.Log($"REQUEST => {Const.BASE_URL_WORLD_TIME}");

            var data = (await request.SendWebRequest()).downloadHandler.text;

            Debug.Log($"DATA => {data}");
            return JsonConvert.DeserializeObject<WorldTime>(data);
        }

        public static Item ResourceItem(ItemData id)
        {
            return ResourceItemsDictionaries.TryGetValue(id.Id, out var x) ? x : null;
        }

        public static Map ResourceMap(MapData md)
        {
            return ResourceMapsDictionaries.TryGetValue(md.Id, out var x) ? x : null;
        }

        public static void UpdateQuest()
        {
            Template = SelectedMapData.Quests;

            foreach (var x in SelectedMap.quests)
            {
                foreach (var y in SelectedMapData.Quests.Where(y => x.id == y.Id))
                {
                    y.Set(x);
                    break;
                }
            }

            foreach (var x in User.Maps.Where(x => x.Id.Equals(SelectedMapData.Id)))
            {
                foreach (var y in x.Quests)
                {
                    foreach (var z in SelectedMapData.Quests.Where(z => y.Id.Equals(z.Id)))
                    {
                        foreach (var k in y.RequireData)
                        {
                            foreach (var l in z.RequireData.Where(l => k.IdItemRequire.Equals(l.IdItemRequire)))
                            {
                                k.CurrentAmount = l.CurrentAmount;
                                k.RequireAmount = l.RequireAmount;
                                break;
                            }
                        }

                        y.Received = z.Received;
                        break;
                    }
                }

                break;
            }
        }

        public static void RevokeUpdateQuest()
        {
            if (Template.Count < 1)
            {
                return;
            }

            foreach (var x in Template)
            {
                foreach (var y in SelectedMapData.Quests.Where(y => x.Id == y.Id))
                {
                    y.Set(x);
                    break;
                }
            }

            foreach (var x in User.Maps.Where(x => x.Id.Equals(SelectedMapData.Id)))
            {
                foreach (var y in x.Quests)
                {
                    foreach (var z in Template.Where(z => y.Id.Equals(z.Id)))
                    {
                        foreach (var k in y.RequireData)
                        {
                            foreach (var l in z.RequireData.Where(l => k.IdItemRequire.Equals(l.IdItemRequire)))
                            {
                                k.CurrentAmount = l.CurrentAmount;
                                k.RequireAmount = l.RequireAmount;
                                break;
                            }
                        }

                        y.Received = z.Received;
                        break;
                    }
                }

                break;
            }

            Template.Clear();
        }

        public static void SetUser(User user)
        {
            if (User == null)
            {
                User = user;

                AddAllAsync();

                QuestManager.Instance.Initialize();
            }
            else
            {
                User = user;
            }

            GameEvent.DoUpdateResource();
        }

        public static void SetPet(ItemData pet)
        {
            var oldPet = User.Pet;

            User.Pet = pet;

            UpdateUser(() => User.Pet = oldPet, () => { }).Forget();
        }

        public static void SetHook(ItemData hook)
        {
            var oldHook = User.Hook;

            User.Hook = hook;

            UpdateUser(() => User.Hook = oldHook, () => { }).Forget();
        }

        public static void SetLine(ItemData line)
        {
            var oldLine = User.Line;

            User.Line = line;

            UpdateUser(() => User.Line = oldLine, () => { }).Forget();
        }

        public static void SetSkin(ItemData skin)
        {
            var oldSkin = User.Skin;

            User.Skin = skin;

            UpdateUser(() => User.Skin = oldSkin, () => { }).Forget();
        }

        public static async void SetConfig(Config config)
        {
            var worldTime = await GetWorldTime();

            Config = config;

            if (worldTime.Unixtime > long.Parse(config.TimerMaintenance))
            {
                return;
            }

            Instance.SetGameState(GameState.Pause);

            if (SceneManager.CurrentScene == "Game Menu")
            {
                UIManager.Instance.HideAllAndShowUI<UIMaintenance>();
            }
            else
            {
                SceneManager.LoadScene("Game Menu");
            }
        }

        public static void SetSelectedLevel(Level level, int index)
        {
            SelectedLevel = level;
            SelectedLevelIndex = index;
        }

        public static void SetSelectedMap(Map map)
        {
            SelectedMap = map;

            for (var i = 0; i < User.Maps.Count; i++)
            {
                if (User.Maps[i].Id != map.id)
                {
                    continue;
                }

                SelectedMapData = User.Maps[i];
                SelectedMapDataIndex = i;
                return;
            }
        }

        public static bool CheckSpentDiamond(float price)
        {
            if (!(User.Diamond >= price))
            {
                return false;
            }

            User.Diamond -= price;
            return true;
        }

        public static bool CheckSpentEnergy(int price)
        {
            if (User.Energy < price)
            {
                return false;
            }

            User.Energy -= price;
            return true;
        }

        public static bool CheckSpentGold(float price)
        {
            if (!(User.Gold >= price))
            {
                return false;
            }

            User.Gold -= price;
            return true;
        }

        public static async UniTask UpdateUser(Action onUpdateFailed, Action onUpdateSuccess)
        {
            var update = await FirebaseAPI.Instance.SetAsync(Const.PATH_USER, $"userId-{User.Id}", User);

            if (update.Contains("error"))
            {
                onUpdateFailed();
            }
            else
            {
                onUpdateSuccess();
            }
        }
    }
}