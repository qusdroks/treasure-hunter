﻿using System;
using GameAssets.Scripts.Model;
using UnityEngine;
using SceneManager = GameAssets.Scripts.Manager.SceneManager;

namespace GameAssets.Scripts.Event
{
    [Serializable]
    public class Collection
    {
        public string id;

        public int amount;

        public bool gold;
    }

    public class GameEvent : MonoBehaviour
    {
        [SerializeField] private bool update = true;
        
        #region Delegate

        #region Base Delegate

        public delegate void Win();

        public delegate void Draw();

        public delegate void Lose();

        public delegate void ChangeLanguage(SystemLanguage language);

        public delegate void FailedInternet();

        public delegate void ConnectedInternet();

        public delegate void StopBaseCharacter();

        #endregion

        public delegate void OpenNewMap(string id);

        public delegate bool UseBomb();

        public delegate bool UseFirecracker();

        public delegate void HookShoot();

        public delegate void HookRotation();

        public delegate void ChangeSpeed(float percent, bool add);

        public delegate void ChangeOriginSpeed(float percent);

        public delegate void Dispute(float dmg, bool useBomb);

        public delegate void UpdateResource();

        public delegate void QuestCollection(Collection id);

        public delegate void QuestCompleteMap(Map map);

        public delegate void QuestCompleteLevel(Level level);

        public delegate void QuestKill(string id);

        #endregion

        #region Static Event

        #region Base Event

        public static event Win OnWin;
        public static event Draw OnDraw;
        public static event Lose OnLose;

        public static event ChangeLanguage OnChangeLanguage;
        public static event FailedInternet OnFailedInternet;
        public static event ConnectedInternet OnConnectedInternet;
        public static event StopBaseCharacter OnStopBaseCharacter;

        #endregion

        public static event OpenNewMap OnOpenNewMap;
        public static event UseBomb OnUseBomb;
        public static event UseFirecracker OnUseFirecracker;

        public static event HookShoot OnHookShoot;
        public static event HookRotation OnHookRotation;

        public static event ChangeSpeed OnChangeSpeed;
        public static event ChangeOriginSpeed OnChangeOriginSpeed;

        public static event Dispute OnDispute;
        public static event UpdateResource OnUpdateResource;

        public static event QuestCollection OnQuestCollection;
        public static event QuestCompleteMap OnQuestCompleteMap;
        public static event QuestCompleteLevel OnQuestCompleteLevel;
        public static event QuestKill OnQuestKill;

        #endregion

        #region Base Handle Event

        private void Update()
        {
            if (!update)
            {
                return;
            }
            
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                DoFailedInternet();
                return;
            }

            DoConnectedInternet();

#if UNITY_EDITOR
            if (!SceneManager.CurrentScene.Contains("Game Play"))
            {
                return;
            }

            if (Input.GetKeyDown(KeyCode.W))
            {
                DoWin();
            }
            else if (Input.GetKeyDown(KeyCode.L))
            {
                DoLose();
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                DoDraw();
            }
#endif
        }

        public static void DoWin()
        {
            OnWin?.Invoke();
        }

        public static void DoDraw()
        {
            OnDraw?.Invoke();
        }

        public static void DoLose()
        {
            OnLose?.Invoke();
        }

        public static void DoChangeLanguage(SystemLanguage language)
        {
            OnChangeLanguage?.Invoke(language);
        }

        public static void DoFailedInternet()
        {
            OnFailedInternet?.Invoke();
        }

        public static void DoConnectedInternet()
        {
            OnConnectedInternet?.Invoke();
        }

        public static void DoStopBaseCharacter()
        {
            OnStopBaseCharacter?.Invoke();
        }

        #endregion

        public static void DoHookShoot()
        {
            OnHookShoot?.Invoke();
        }

        public static void DoDispute(float dmg, bool useBomb)
        {
            OnDispute?.Invoke(dmg, useBomb);
        }

        public static void DoQuestCollection(Collection collection)
        {
            OnQuestCollection?.Invoke(collection);
        }

        public static void DoQuestCompleteMap(Map map)
        {
            OnQuestCompleteMap?.Invoke(map);
        }

        public static void DoQuestCompleteLevel(Level level)
        {
            OnQuestCompleteLevel?.Invoke(level);
        }

        public static void DoQuestKill(string id)
        {
            OnQuestKill?.Invoke(id);
        }

        public static void DoHookRotation()
        {
            OnHookRotation?.Invoke();
        }

        public static void DoUpdateResource()
        {
            OnUpdateResource?.Invoke();
        }

        public static void DoOpenNewMap(string id)
        {
            OnOpenNewMap?.Invoke(id);
        }

        public static void DoChangeSpeed(float percent, bool add)
        {
            OnChangeSpeed?.Invoke(percent, add);
        }

        public static void DoChangeOriginSpeed(float percent)
        {
            OnChangeOriginSpeed?.Invoke(percent);
        }

        public static bool DoUseBomb()
        {
            return OnUseBomb != null && OnUseBomb();
        }

        public static bool DoUseFirecracker()
        {
            return OnUseFirecracker != null && OnUseFirecracker();
        }
    }
}