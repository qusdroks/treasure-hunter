﻿namespace GameAssets.Scripts.General
{
    public enum VibrationType
    {
        Selection,
        Success,
        Warning,
        Failure,
        LightImpact,
        MediumImpact,
        HeavyImpact
    }

    public enum CharacterState
    {
        IdleOrRespawn = 0,
        Move = 1,
        Attack = 2,
        Death = 3
    }

    public enum GameState
    {
        Lobby,
        Loading,
        Pause,
        Playing,
        Lose,
        Win,
        Draw,
        LostConnected,
        ReConnected
    }

    public enum UIType
    {
        None,
        UIPlay,
        UIGameStart,
        UIGameComplete,
        UIGameOver,
        UIGameDraw,
        UIShop,
        UILoading,
        UISpin,
        UISelectMap,
        UISelectLevel,
        UIQuest,
        UIMapInfo,
        UIChallenge,
        UISquid,
        UIInventory,
        UIReward,
        UISetting,
        UIMaintenance,
        UIPause,
        UITooltip,
        UIPlayerInfo
    }

    public enum FitType
    {
        Uniform,
        Width,
        Height,
        FixedRows,
        FixedColumns,
        FixedBoth
    }

    public enum Alignment
    {
        Horizontal,
        Vertical
    }

    public enum PriorityLevel
    {
        Low,
        Normal,
        High
    }

    public enum HookAction
    {
        Rotation,
        Shoot,
        Rewind
    }

    public enum BoosterType
    {
        Bomb,
        Firecracker,
        Strength,
        Lucky,
        Timer
    }

    public enum ValidatorType
    {
        None,
        String,
        Email,
        Password,
        ConfirmPassword
    }

    public enum FormatType
    {
        LowerNumber,
        LowerNumberSpecial,
        UpperLowerNumber,
        UpperLowerNumberSpecial
    }

    public enum AngleDirection
    {
        None,
        Up,
        Down,
        Left,
        Right
    }

    public enum ItemType
    {
        Hook,
        Line,
        Pet,
        Treasure,
        Piece,
        Booster,
        Skin,
        Craft,
        Gold,
        Stone,
        Diamond,
        Energy,
        Other
    }

    public enum AnimationType
    {
        Idle = 0,
        Open = 1,
        Move = 1,
        Dispute = 2,
        Run = 2,
        Jump = 3,
        Fall = 4,
        Attack = 5,
        Death = 6
    }

    public enum QuestType
    {
        Collection,
        CompleteMap,
        CompleteLevel,
        Kill
    }

    public enum FilterType
    {
        // FILTER QUEST
        Daily,
        Pet,
        Treasure,
        Special,
        Player,
        Other,
        
        // FILTER SHOP
        Shop,
        DailyShop,
        
        // FILTER INVENTORY
        Hook,
        Skin,
        Line,
        None
    }
}