﻿using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.UI.UIPlay.GameMenu;

namespace GameAssets.Scripts.UI.UISelectMap
{
    public partial class UISelectMap
    {
        private void Awake()
        {
            btnBack.AddListener(() => UIManager.Instance.HideAllAndShowUI<UIPlaySgm>());
            btnShop.AddListener(() => UIManager.Instance.HideAllAndShowUI<UIInventory.UIInventory>());
            
            AudioManager.Instance.PlayMusic("map-shop2");
        }
    }
}