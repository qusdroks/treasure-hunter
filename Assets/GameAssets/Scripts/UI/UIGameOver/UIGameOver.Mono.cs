using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.UI.UIPlay.GamePlay;

namespace GameAssets.Scripts.UI.UIGameOver
{
    public partial class UIGameOver
    {
        private void Awake()
        {
            _uiPlaySgp = UIManager.Instance.GetUI<UIPlaySgp>();
            btnHome.onClick.AddListener(() => Click(() => SceneManager.LoadScene("Game Menu")));

            btnRestart.onClick.AddListener(() =>
            {
                if (GameManager.CheckSpentEnergy(GameManager.SelectedLevel.energy))
                {
                    SceneManager.ResetCurrentScene();
                }
                else
                {
                    SpawnerEx.CreateNotification("Bạn không đủ năng lượng");
                }
            });

            txtScore.text =
                $"<size=55>Điểm của bạn</size>{UIEx.NewLine}<color=#dd5900>{_uiPlaySgp.Gold}/{_uiPlaySgp.Target}</color>";
            
            AudioManager.Instance.PlaySound("win");
        }
    }
}