using GameAssets.Scripts.Base;
using GameAssets.Scripts.UI.UIPlay.GamePlay;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.UI.UIGameOver
{
    public partial class UIGameOver : BaseUI
    {
        [SerializeField] private Button btnHome;
        [SerializeField] private Button btnRestart;

        [SerializeField] private TextMeshProUGUI txtScore;

        private UIPlaySgp _uiPlaySgp;
    }
}