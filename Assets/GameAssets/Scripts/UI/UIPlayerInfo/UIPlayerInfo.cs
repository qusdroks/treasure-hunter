using GameAssets.Scripts.Base;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.UI.UIPlayerInfo
{
    public class UIPlayerInfo : BaseUI
    {
        [SerializeField] private GameObject lbInfo;
        [SerializeField] private GameObject lbSetting;

        [SerializeField] private Button btnInfo;
        [SerializeField] private Button btnSetting;
        [SerializeField] private Button btnBack;

        private RectTransform _rtSetting;
        private RectTransform _rtInfo;

        private Vector2 _rtSettingAnchorOrigin;
        private Vector2 _rtSettingSizeOrigin;
        private Vector2 _rtInfoAnchorOrigin;
        private Vector2 _rtInfoSizeOrigin;

        private void Awake()
        {
            btnInfo.onClick.AddListener(() => SetLabel(true));
            btnSetting.onClick.AddListener(() => SetLabel(false));
            btnBack.onClick.AddListener(() => gameObject.SetActive(false));

            _rtSetting = btnSetting.GetComponent<RectTransform>();
            _rtInfo = btnInfo.GetComponent<RectTransform>();

            _rtSettingAnchorOrigin = _rtSetting.anchoredPosition;
            _rtSettingSizeOrigin = _rtSetting.sizeDelta;
            _rtInfoAnchorOrigin = _rtInfo.anchoredPosition;
            _rtInfoSizeOrigin = _rtInfo.sizeDelta;
        }

        private void OnEnable()
        {
            btnInfo.interactable = false;

            SetLabel(true);
        }

        private void SetLabel(bool value)
        {
            btnInfo.interactable = !value;
            btnSetting.interactable = value;

            lbInfo.SetActive(value);
            lbSetting.SetActive(!value);

            if (!value)
            {
                _rtSetting.anchoredPosition = new Vector2(_rtSetting.anchoredPosition.x, -50f);
                _rtSetting.sizeDelta = new Vector2(_rtSetting.sizeDelta.x, 75f);

                _rtInfo.anchoredPosition = _rtInfoAnchorOrigin;
                _rtInfo.sizeDelta = _rtInfoSizeOrigin;
            }
            else
            {
                _rtInfo.anchoredPosition = new Vector2(_rtInfo.anchoredPosition.x, -50f);
                _rtInfo.sizeDelta = new Vector2(_rtInfo.sizeDelta.x, 75f);

                _rtSetting.anchoredPosition = _rtSettingAnchorOrigin;
                _rtSetting.sizeDelta = _rtSettingSizeOrigin;
            }
        }

        public new static void SignOut()
        {
            BaseUI.SignOut();
        }
    }
}