﻿using Cysharp.Threading.Tasks;
using GameAssets.Scripts.GamePlay;
using GameAssets.Scripts.Manager.Game;
using TMPro;
using UnityEngine;
using Button = UnityEngine.UI.Button;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.UI.UIPlayerInfo
{
    public class PlayerInfoController : MonoBehaviour
    {
        [SerializeField] private Transform starParent;

        [SerializeField] private Button btnEdit;

        [SerializeField] private TMP_InputField inputName;

        [SerializeField] private TextMeshProUGUI txtId;
        [SerializeField] private TextMeshProUGUI txtLevel;

        private void Awake()
        {
            inputName.readOnly = true;
            txtId.text = GameManager.User.Id;
            txtLevel.text = $"{Random.Range(0, 15)}";

            btnEdit.onClick.AddListener(() => inputName.readOnly = false);

            inputName.onEndEdit.AddListener(value =>
            {
                inputName.readOnly = true;
                GameManager.User.Username = value;

                GameManager.UpdateUser(() => { }, () => { }).Forget();
            });
        }

        private void OnEnable()
        {
            inputName.text = GameManager.User.Username;

            StarController.Instance.Initialize(starParent, Random.Range(1, 3));
        }
    }
}