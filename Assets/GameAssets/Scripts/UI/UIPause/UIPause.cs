using GameAssets.Scripts.Base;
using GameAssets.Scripts.General;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.UI.UIPause
{
    public partial class UIPause : BaseUI
    {
        [SerializeField] private GameAssets.Scripts.GamePlay.Button btnHome;
        [SerializeField] private GameAssets.Scripts.GamePlay.Button btnPlay;

        [SerializeField] private Image imgPause;

        [SerializeField] private Sprite icPause;

        private GameState _gameState;
    }
}
