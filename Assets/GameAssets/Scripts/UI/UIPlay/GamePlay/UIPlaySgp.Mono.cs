using System.Linq;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.GamePlay.Player;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model.Item;
using UnityEngine;

namespace GameAssets.Scripts.UI.UIPlay.GamePlay
{
    public partial class UIPlaySgp
    {
        private void Awake()
        {
            GameEvent.OnUpdateResource += UpdateResource;

            btnHookShoot.onClick.AddListener(GameEvent.DoHookShoot);

            btnPause.onClick.AddListener(() =>
            {
                UIManager.Instance.ShowUI<UIPause.UIPause>().GetCurrentState();
                GameManager.Instance.SetGameState(GameState.Pause);

                imgPause.sprite = icResume;
            });

            btnDestroyItem.onClick.AddListener(() =>
            {
                Player.Instance.Hook.DestroyItem(Player.Instance.MorePercentDestroyItem());

                btnDestroyItem.gameObject.SetActive(false);
            });

            var time = GameManager.SelectedLevel.time;

            foreach (var x in GameManager.User.Items.Select(GameManager.ResourceItem))
            {
                if (!(x is Booster {boosterType: BoosterType.Timer} b))
                {
                    continue;
                }

                time += Mathf.RoundToInt(b.timer);

                GameManager.User.RemoveItem(x, 1);
                break;
            }

            _second = time;
            Target = GameManager.SelectedLevel.completed
                ? GameManager.SelectedLevel.gold1
                : GameManager.SelectedLevel.gold2;

            txtTime.text = $"{_second:00}";
            txtLevel.text = $"Lv {GameManager.SelectedLevel.level}";

            var areaT = playArea.transform;

            GameManager.UpdateUser(() => { }, () => { }).Forget();
            GameEvent.DoUpdateResource();

            DrawItem();

            foreach (var x in GameManager.SelectedMap.treasures)
            {
                var rnd = UnityEngine.Random.Range(0, 100f);

                if (rnd >= x.showChance)
                {
                    continue;
                }

                var go = SpawnerEx.CreateSpawner(Random, areaT, treasurePrefab);

                go.GetComponent<Scripts.GamePlay.Resource.Treasure>()?.Initiation(x, areaT, true);
                go.gameObject.AddComponent<PolygonCollider2D>().isTrigger = true;

                var gt = go.transform;

                gt.localScale = Vector3.one / 2f;
                gt.position = new Vector3(Random.x, Random.y, -0.01f);
            }
        }

        private void Start()
        {
            _totalFirecracker = Player.Instance.MoreFirecracker();
            _totalStrength = Player.Instance.MoreStrength();
        }

        private void OnDestroy()
        {
            GameEvent.OnUpdateResource -= UpdateResource;
        }

        private void Update()
        {
            _second -= Time.deltaTime / slowTime;

            if (!(_second <= 0f))
            {
                txtTime.text = $"{_second:0}s";
            }
            else
            {
                UIManager.Instance.HideUI<UIChallenge.UIChallenge>();
                UIManager.Instance.HideUI<UISquid.UISquid>();

                if (Gold >= Target)
                {
                    GameEvent.DoWin();
                }
                else
                {
                    GameEvent.DoLose();
                }
            }
        }
    }
}