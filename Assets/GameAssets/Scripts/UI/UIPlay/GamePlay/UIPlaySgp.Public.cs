using DG.Tweening;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model.Item;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.GamePlay.Player;
using GameAssets.Scripts.Manager.Audio;
using Booster = GameAssets.Scripts.GamePlay.Booster;

namespace GameAssets.Scripts.UI.UIPlay.GamePlay
{
    public partial class UIPlaySgp
    {
        public async UniTask UpdateResource(Vector3 position, Resource r)
        {
            var value = new KeyValuePair<float, float>();
            var target = Vector3.zero;

            if (r != null && r.clip != null)
            {
                AudioManager.Instance.PlaySound(r.clip.name);
            }

            switch (r.itemType)
            {
                case ItemType.Diamond:
                    value = Player.Instance.MoreTimeAndDiamond(r.value);
                    target = txtDiamond.transform.position;
                    break;

                case ItemType.Gold:
                    value = Player.Instance.MoreTimeAndGold(r.value);
                    target = txtGold.transform.position;
                    break;

                case ItemType.Stone:
                    value = Player.Instance.MoreTimeAndStone(r.value);
                    target = txtGold.transform.position;
                    break;
            }

            var clone = SpawnerEx.CreateCombat(position, $"+{value.Key}");
            var stonePercent = Player.Instance.MoreStonePercent();
            var rnd = UnityEngine.Random.Range(0, stonePercent);

            Gold += value.Key;

            await clone.transform
                .DOMove(GameManager.Camera.ScreenToWorldPoint(
                    new Vector3(target.x, target.y, GameManager.Camera.nearClipPlane)), lerpSpeed)
                .OnComplete(() =>
                {
                    switch (r.itemType)
                    {
                        case ItemType.Diamond:
                            txtDiamond.NumberCounter(float.Parse(txtDiamond.text), value.Key);
                            break;

                        case ItemType.Gold:
                            UpdateGold(value.Key);
                            break;

                        case ItemType.Stone:
                            UpdateGold(value.Key * (rnd < stonePercent ? Player.Instance.MoreStoneValue() : 1f));
                            break;
                    }

                    ChangeTime(value.Value);

                    GameEvent.DoQuestCollection(new Collection
                    {
                        id = r.id,
                        amount = (int) r.value,
                        gold = true
                    });

                    SpawnerEx.DestroySpawner(clone);
                    GameManager.User.AddItem(r, r.value);
                })
                .AsyncWaitForCompletion()
                .AsUniTask();
        }

        public void UpdateGold(float gold)
        {
            txtGold.NumberCounter(float.Parse(txtGold.text), gold);
        }

        public void DrawItem()
        {
            var boosters = new Dictionary<Model.Item.Booster, int>();
            var boosterFirecracker = false;
            var boosterStrength = false;

            foreach (var x in GameManager.User.Items)
            {
                var y = GameManager.ResourceItem(x);
                var b = y as Model.Item.Booster;

                if (b == null)
                {
                    continue;
                }

                switch (b.boosterType)
                {
                    case BoosterType.Lucky:
                    case BoosterType.Timer:
                        continue;

                    case BoosterType.Firecracker:
                        boosterFirecracker = true;

                        if (!boosters.ContainsKey(b))
                        {
                            boosters.Add(b, x.Amount + _totalFirecracker);
                        }

                        break;

                    case BoosterType.Strength:
                        boosterStrength = true;

                        if (!boosters.ContainsKey(b))
                        {
                            boosters.Add(b, x.Amount + _totalStrength);
                        }

                        break;

                    case BoosterType.Bomb:
                        if (!boosters.ContainsKey(b))
                        {
                            boosters.Add(b, x.Amount);
                        }

                        break;
                }
            }

            foreach (var x in boosterParent.GetComponentsInChildren<Booster>())
            {
                SpawnerEx.DestroySpawner(x.transform);
            }

            foreach (var x in boosters.Where(x => x.Value >= 1))
            {
                SpawnerEx.CreateUISpawner(boosterParent, boosterPrefab)
                    .GetComponent<Booster>()?.Initiation(x.Key, x.Value);
            }

            if (!boosterStrength && _totalStrength > 0)
            {
                SpawnerEx.CreateUISpawner(boosterParent, boosterPrefab)
                    .GetComponent<Booster>()
                    ?.Initiation(icBoosterStrength, _totalStrength, BoosterType.Strength);
            }

            if (!boosterFirecracker && _totalFirecracker > 0)
            {
                SpawnerEx.CreateUISpawner(boosterParent, boosterPrefab)
                    .GetComponent<Booster>()
                    ?.Initiation(icBoosterFirecracker, _totalFirecracker, BoosterType.Firecracker);
            }
        }

        public Vector3 Random => playArea.bounds.Random();

        public void RemoveTotalFirecracker()
        {
            _totalFirecracker -= 1;
        }

        public void RemoveTotalStrength()
        {
            _totalStrength -= 1;
        }

        public void ChangeTime(float second)
        {
            _second -= second;
            txtTime.text = $"{_second:00}";
        }

        public void SetDestroyItem(bool value)
        {
            btnDestroyItem.gameObject.SetActive(value);
        }
    }
}