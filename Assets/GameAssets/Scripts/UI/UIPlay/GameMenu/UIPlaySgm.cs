using DG.Tweening;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.GamePlay;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Model;
using Tamarin.FirebaseX;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.UI.UIPlay.GameMenu
{
    public partial class UIPlaySgm : BaseUI
    {
        [Header("[OTHER]")] [SerializeField] private Default defaults;
        [SerializeField] private CanvasGroup mainPanel;

        [SerializeField] private FormFieldEx formRegister;
        [SerializeField] private FormFieldEx formLogin;

        [Header("[GAME OBJECT]")] [SerializeField]
        private GameObject loginPanel;

        [SerializeField] private GameObject registerPanel;

        [Header("[BUTTON]")] [SerializeField] private Button btnLLogin;
        [SerializeField] private UnityEngine.UI.Button btnLRegister;
        [SerializeField] private UnityEngine.UI.Button btnRLogin;
        [SerializeField] private Button btnRRegister;
        [SerializeField] private Button btnPlay;

        [Header("[Input Field]")] [SerializeField]
        private TMP_InputField inputUsernameLogin;

        [SerializeField] private TMP_InputField inputPasswordLogin;
        [SerializeField] private TMP_InputField inputUsernameRegister;
        [SerializeField] private TMP_InputField inputPasswordRegister;
        [SerializeField] private TMP_InputField inputConfirmPasswordRegister;
        [SerializeField] private TMP_InputField inputReferenceRegister;

        private Transform _txtLogin;
        private Transform _txtRegister;

        private Transform _imgLoginLoading;
        private Transform _imgRegisterLoading;

        private ValidatorEx _emailLoginValidator;
        private ValidatorEx _passwordLoginValidator;

        private ValidatorEx _emailRegisterValidator;
        private ValidatorEx _passwordRegisterValidator;
        private ValidatorEx _confirmPasswordRegisterValidator;

        private void ResetInput()
        {
            _emailLoginValidator.ResetError();
            _passwordLoginValidator.ResetError();

            _emailRegisterValidator.ResetError();
            _passwordRegisterValidator.ResetError();
            _confirmPasswordRegisterValidator.ResetError();

            inputReferenceRegister.text = string.Empty;
        }

        private void ReadOnly(bool value)
        {
            inputUsernameLogin.readOnly = value;
            inputPasswordLogin.readOnly = value;

            inputUsernameRegister.readOnly = value;
            inputPasswordRegister.readOnly = value;

            btnLRegister.interactable = !value;
            btnRLogin.interactable = !value;

            btnLLogin.SetInteractable(!value);
            btnRRegister.SetInteractable(!value);

            _txtLogin.gameObject.SetActive(!value);
            _txtRegister.gameObject.SetActive(!value);

            _imgLoginLoading.gameObject.SetActive(value);
            _imgRegisterLoading.gameObject.SetActive(value);

            if (!value)
            {
                return;
            }

            _imgLoginLoading.DOKill();
            _imgRegisterLoading.DOKill();

            _imgLoginLoading.DORotate(Vector3.back, 0f)
                .SetLoops(-1, LoopType.Incremental)
                .From(Vector3.zero);

            _imgRegisterLoading.DORotate(Vector3.back, 0f)
                .SetLoops(-1, LoopType.Incremental)
                .From(Vector3.zero);
        }

        private void ShowError(string error)
        {
            SpawnerEx.CreateNotification(error);

            ReadOnly(false);
            ResetInput();
        }

        private void ReturnLogin(bool value)
        {
            mainPanel.blocksRaycasts = value;

            loginPanel.SetActive(!value);
        }

        private async void OnRegisterSuccess()
        {
            isSignedIn = false;

            var user = await FirebaseAPI.Instance.RegisterWithEmail($"{inputUsernameRegister.text}@gmail.com",
                inputPasswordRegister.text);

            if (user != null)
            {
                if (user.status)
                {
                    var userModel = User.Create(defaults, inputPasswordRegister.text,
                        inputReferenceRegister.text, inputUsernameRegister.text, user.UserId);

                    var result = await FirebaseAPI.Instance.SetAsync(Const.PATH_USER,
                        $"userId-{user.UserId}", userModel);

                    if (result.Contains("error"))
                    {
                        isSignedIn = true;

                        ShowError("Đăng ký lỗi");
                        return;
                    }

                    ShowError("Đăng ký thành công");

                    loginPanel.SetActive(true);
                    registerPanel.SetActive(false);
                }
                else
                {
                    isSignedIn = true;

                    ShowError("Đăng ký lỗi, thử lại");
                }
            }
            else
            {
                isSignedIn = true;

                ShowError("Đăng ký lỗi, thử lại");
            }
        }
    }
}