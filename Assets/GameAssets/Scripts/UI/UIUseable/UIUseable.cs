using GameAssets.Scripts.Base;
using GameAssets.Scripts.Model.Item;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Button = GameAssets.Scripts.GamePlay.Button;

namespace GameAssets.Scripts.UI.UIUseable
{
    public class UIUseable : BaseUI
    {
        [SerializeField] private Image imgTreasure;
        
        [SerializeField] private Button btnInc;
        [SerializeField] private Button btnDec;
        [SerializeField] private Button btnMax;
        [SerializeField] private Button btnUse;
        [SerializeField] private UnityEngine.UI.Button btnBack;

        [SerializeField] private TMP_InputField inputAmount;

        private int _amount;

        public void Initialize(Treasure treasure, int amount)
        {
            imgTreasure.sprite = treasure.icon;
            
            btnInc.AddListener(() => UpdateAmount(true, amount));
            btnDec.AddListener(() => UpdateAmount(false, amount));
            btnMax.AddListener(() => UpdateAmount(amount));
            btnUse.AddListener(() => treasure.Use(_amount));

            btnBack.onClick.RemoveAllListeners();
            btnBack.onClick.AddListener(() => gameObject.SetActive(false));

            inputAmount.onEndEdit.RemoveAllListeners();

            inputAmount.onEndEdit.AddListener(value =>
            {
                if (int.TryParse(value, out var number))
                {
                    UpdateAmount(number >= amount ? amount : number);
                }
                else
                {
                    UpdateAmount(0);
                }
            });
        }

        private void UpdateAmount(int amount)
        {
            _amount = amount;

            UpdateText();
        }

        private void UpdateAmount(bool value, int max)
        {
            if (value)
            {
                if (_amount + 1 > max)
                {
                    return;
                }

                _amount += 1;

                UpdateText();
            }
            else
            {
                if (_amount - 1 < 0)
                {
                    return;
                }

                _amount -= 1;

                UpdateText();
            }
        }

        private void UpdateText()
        {
            inputAmount.text = $"{_amount}";
        }
    }
}