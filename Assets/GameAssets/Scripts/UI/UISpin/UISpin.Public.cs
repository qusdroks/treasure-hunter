﻿using DG.Tweening;
using GameAssets.Scripts.Manager.Game;

namespace GameAssets.Scripts.UI.UISpin
{
    public partial class UISpin
    {
        public void UpdateLevel()
        {
            var spin = GameManager.User.LevelSpin + 1;

            txtLevelSpin.text = $"Lv {spin}";

            if (GameManager.Config == null)
            {
                return;
            }

            txtSpinGold.text = $"Vàng                        :{spin * GameManager.Config.SpinGold}%";
            txtSpinItemsInShop.text = $"Vật phẩm trong shop :{spin * GameManager.Config.SpinItemsInShop}%";
            txtSpinRareItems.text = $"Vật phẩm hiếm          :{spin * GameManager.Config.SpinRareItems}%";

            txtLevelUpSpinGold.text = $"Vàng                        :{(spin + 1) * GameManager.Config.SpinGold}%";
            txtLevelUpSpinItemsInShop.text = $"Vật phẩm trong shop :{(spin + 1) * GameManager.Config.SpinItemsInShop}%";
            txtLevelUpSpinRareItems.text = $"Vật phẩm hiếm          :{(spin + 1) * GameManager.Config.SpinRareItems}%";
        }

        public void UpdateFill()
        {
            imgLevelExpFill.DOFillAmount(GameManager.User.CurrentExpLevelSpin / GameManager.User.MaxExpLevelSpin, 0.5f);
        }
    }
}