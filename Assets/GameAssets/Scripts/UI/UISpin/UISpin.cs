using GameAssets.Scripts.Base;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.GamePlay;
using GameAssets.Scripts.Manager.Game;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.UI.UISpin
{
    public partial class UISpin : BaseUI
    {
        [Header("[OTHER]")] [SerializeField] private PickerWheel pickerWheel;

        [Header("[BUTTON]")] [SerializeField] private GamePlay.Button btnBack;
        [SerializeField] private GamePlay.Button btnSpin;

        [Header("[IMAGE]")] [SerializeField] private Image imgLevelExpFill;

        [Header("[TEXT]")] [SerializeField] private TextMeshProUGUI txtLevelSpin;
        [SerializeField] private TextMeshProUGUI txtSpinGold;
        [SerializeField] private TextMeshProUGUI txtSpinItemsInShop;
        [SerializeField] private TextMeshProUGUI txtSpinRareItems;

        [SerializeField] private TextMeshProUGUI txtLevelUpSpinGold;
        [SerializeField] private TextMeshProUGUI txtLevelUpSpinItemsInShop;
        [SerializeField] private TextMeshProUGUI txtLevelUpSpinRareItems;

        [SerializeField] private TextMeshProUGUI txtGold;
        [SerializeField] private TextMeshProUGUI txtDiamond;
        [SerializeField] private TextMeshProUGUI txtEnergy;

        private void UpdateResource()
        {
            txtGold.NumberCounter(0f, GameManager.User.Gold);
            txtDiamond.NumberCounter(0f, GameManager.User.Diamond);
            
            txtEnergy.text = $"{GameManager.User.Energy}/50";
        }
    }
}