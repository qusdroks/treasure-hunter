using System.Collections.Generic;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Model;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.UI.UIQuest
{
    public partial class UIQuest : BaseUI
    {
        [SerializeField] private List<Filter> filters = new List<Filter>();

        [SerializeField] private Button btnBack;

        private int _index;
    }
}