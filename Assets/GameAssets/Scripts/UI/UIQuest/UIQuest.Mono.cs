﻿namespace GameAssets.Scripts.UI.UIQuest
{
    public partial class UIQuest
    {
        private void Awake()
        {
            btnBack.onClick.AddListener(() => gameObject.SetActive(false));

            foreach (var x in filters)
            {
                x.btnFilter.onClick.RemoveAllListeners();
                x.btnFilter.onClick.AddListener(() => DrawQuest(x.filterType));
            }
        }
    }
}