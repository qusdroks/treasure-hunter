using System;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.Manager.Game;
using Sirenix.Utilities;

namespace GameAssets.Scripts.UI.UISelectLevel
{
    public partial class UISelectLevel
    {
        private void Awake()
        {
            btnBack.AddListener(() =>
            {
                AudioManager.Instance.PlayMusic("map-shop2");
                UIManager.Instance.HideAllAndShowUI<UISelectMap.UISelectMap>();
            });
            
            btnInfo.AddListener(() => UIManager.Instance.ShowUI<UIMapInfo.UIMapInfo>());
            btnQuest.AddListener(() => UIManager.Instance.ShowUI<UIQuest.UIQuest>());
        }

        private void OnEnable()
        {
            for (var i = 0; i < maps.Count; i++)
            {
                maps[i].SetActive(i == GameManager.SelectedMap.index);
            }

            var btn = maps[GameManager.SelectedMap.index].GetComponentsInChildren<GamePlay.Button>();
            
            btn.Sort((a, b) => string.Compare(a.name, b.name, StringComparison.Ordinal));

            for (var i = 0; i < GameManager.SelectedMap.levels.Count; i++)
            {
                var j = i;

                if (j <= btn.Length - 1)
                {
                    btn[j].AddListener(() => Click(() => OpenLevel(GameManager.SelectedMap.levels[j])));
                }
            }

            uiQuest.DrawQuest(FilterType.Daily);
        }
    }
}