﻿using System;
using GameAssets.Scripts.Ex;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameAssets.Scripts.Base
{
    public class BaseHandle : MonoBehaviour, IPointerDownHandler, IDragHandler,
        IPointerUpHandler, IBeginDragHandler, IEndDragHandler
    {
        [ShowIf(nameof(canHold))] [Range(0.15f, 0.5f)] [SerializeField]
        private float holdTime = 0.25f;

        [ShowIf(nameof(canDoubleClick))] [Range(0.25f, 0.5f)] [SerializeField]
        private float clickDelayTime = 0.25f;

        [SerializeField] private bool canHold;
        [SerializeField] private bool canDragDrop;
        [SerializeField] private bool canDoubleClick;
        [SerializeField] private bool useHandleInterface;

        public Action OnDoubleClick { get; set; }
        public Vector3 Origin { get; private set; }
        public Vector3 End { get; private set; }
        public Vector3 Delta { get; private set; }

        public bool Dragged { get; private set; }

        private int clicked;
        private float _holdTime;
        private float _clickTime;

        private bool _holdDown;

        #region MonoBehaviour

        protected virtual void Update()
        {
            switch (useHandleInterface)
            {
                case true when _holdDown:
                {
                    if (_holdTime <= 0f)
                    {
                        return;
                    }

                    _holdTime -= Time.deltaTime;

                    if (!(_holdTime <= 0f))
                    {
                        return;
                    }

                    _holdDown = false;

                    DoHandleHoldComplete();
                    break;
                }

                case false:
                    switch (Application.platform)
                    {
                        case RuntimePlatform.Android:
                        case RuntimePlatform.IPhonePlayer:
                            if (Input.touchCount > 0)
                            {
                                var touch = Input.GetTouch(0);

                                switch (touch.phase)
                                {
                                    case TouchPhase.Began:
                                        DoHandleDown();
                                        break;

                                    case TouchPhase.Moved:
                                    case TouchPhase.Stationary:
                                        DoHandleDrag();
                                        break;

                                    case TouchPhase.Ended:
                                        DoHandleUp();
                                        break;
                                }
                            }

                            break;

                        case RuntimePlatform.WindowsEditor:
                        case RuntimePlatform.WindowsPlayer:
                        case RuntimePlatform.WebGLPlayer:
                            if (Input.GetMouseButtonDown(0))
                            {
                                DoHandleDown();
                            }
                            else if (Input.GetMouseButton(0))
                            {
                                DoHandleDrag();
                            }
                            else if (Input.GetMouseButtonUp(0))
                            {
                                DoHandleUp();
                            }

                            break;
                    }

                    break;
            }
        }

        #endregion

        #region Public

        public void OnPointerDown(PointerEventData eventData)
        {
            if (canHold)
            {
                _holdTime = holdTime;
                _holdDown = true;
            }
            else if (canDragDrop)
            {
                Origin = MathfEx.PlatformPosition;
            }

            if (useHandleInterface)
            {
                DoubleClick();
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (_holdDown)
            {
                _holdDown = false;
            }
            else if (canDragDrop)
            {
                DoHandleUp();
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (canDragDrop)
            {
                DoHandleDrag();
            }
            else if (_holdDown)
            {
                _holdTime = holdTime;

                DoHandleDrag();
            }
        }

        public virtual void OnBeginDrag(PointerEventData eventData)
        {
        }

        public virtual void OnEndDrag(PointerEventData eventData)
        {
        }

        #endregion

        #region Private

        private void DoubleClick()
        {
            if (!canDoubleClick)
            {
                return;
            }

            clicked++;

            if (clicked == 1)
            {
                _clickTime = Time.time;
            }

            if (clicked > 1 && Time.time - _clickTime < clickDelayTime)
            {
                clicked = 0;
                _clickTime = 0;

                Debug.Log($"Double Click: {name}");
                OnDoubleClick?.Invoke();
            }
            else if (clicked > 2 || Time.time - _clickTime > 1)
            {
                clicked = 0;
            }
        }

        #endregion

        #region Protected

        protected virtual void DoHandleDown()
        {
            if (!useHandleInterface)
            {
                DoubleClick();
            }

            Origin = MathfEx.PlatformPosition;

            if (canHold)
            {
                return;
            }

            Dragged = false;
            End = MathfEx.PlatformPosition;
        }

        protected virtual void DoHandleDrag()
        {
            if (!canDragDrop)
            {
                return;
            }

            Dragged = !MathfEx.Equal(MathfEx.PlatformPosition, End);

            if (Dragged)
            {
                Delta = MathfEx.PlatformPosition - End;
                End = MathfEx.PlatformPosition;
            }
            else
            {
                Delta = Vector3.zero;
            }
        }

        protected virtual void DoHandleUp()
        {
            if (Dragged)
            {
                Dragged = false;
            }

            Delta = Vector3.zero;
        }

        protected virtual void DoHandleHoldComplete()
        {
        }

        #endregion
    }
}