﻿namespace GameAssets.Scripts.Base.Interface
{
    public interface ISingleton : IInitialize
    {
        void PreInnerUpdate();
        void InnerUpdate();

        void PreInnerFixedUpdate();
        void InnerFixedUpdate();

        void PreInnerLateUpdate();
        void InnerLateUpdate();
    }
}