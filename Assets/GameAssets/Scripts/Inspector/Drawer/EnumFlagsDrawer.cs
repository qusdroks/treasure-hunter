﻿#if UNITY_EDITOR
using GameAssets.Scripts.Inspector.Attribute;
using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Inspector.Drawer
{
    [CustomPropertyDrawer(typeof(EnumFlagsAttribute))]
    public class EnumFlagsDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
        {
            property.intValue = EditorGUI.MaskField(rect, label, property.intValue, property.enumNames);
        }
    }
}
#endif