using UnityEditor;
using UnityEngine;
using UnityEngine.Profiling;

namespace GameAssets.Scripts.Editor.Hierarchy.Icon
{
    public sealed class Memory : IconBase
    {
        private float _mWidth;
        private static readonly GUIContent Label = new GUIContent();

        public override string Name => "Memory Used";
        public override float Width => _mWidth + 4f;

        private static GUIStyle Style => Styles.ApplyPrefabStyle;

        public override void Init()
        {
            _mWidth = 0f;

            if (!EnhancedHierarchy.IsGameObject)
            {
                return;
            }

            if (Preferences.tooltips && !Preferences.relevantTooltipsOnly)
            {
                Label.tooltip = "Used Memory";
            }
            else
            {
                Label.tooltip = string.Empty;
            }

            var memory = Profiler.GetRuntimeMemorySizeLong(EnhancedHierarchy.CurrentGameObject);

            if (memory == 0)
            {
                return;
            }

            Label.text = EditorUtility.FormatBytes(memory);
            _mWidth = Style.CalcSize(Label).x;
        }

        public override void DoGUI(Rect rect)
        {
            if (_mWidth <= 0f)
            {
                return;
            }

            rect.xMin += 2f;
            rect.xMax -= 2f;

            using (new GUIColor(Styles.BackgroundColorEnabled))
            {
                EditorGUI.LabelField(rect, Label, Style);
            }
        }
    }
}