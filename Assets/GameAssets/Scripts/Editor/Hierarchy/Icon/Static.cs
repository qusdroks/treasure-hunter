using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.Icon
{
    public sealed class Static : IconBase
    {
        protected override IconPosition Side => IconPosition.All;

        public override Texture2D PreferencesPreview => Utility.GetBackground(Styles.StaticToggleStyle, false);

        public override void DoGUI(Rect rect)
        {
            using (new GUIBackgroundColor(EnhancedHierarchy.CurrentGameObject.isStatic
                ? Styles.BackgroundColorEnabled
                : Styles.BackgroundColorDisabled))
            {
                GUI.changed = false;

                GUI.Toggle(rect, EnhancedHierarchy.CurrentGameObject.isStatic, Styles.StaticContent,
                    Styles.StaticToggleStyle);

                if (!GUI.changed)
                {
                    return;
                }

                var isStatic = !EnhancedHierarchy.CurrentGameObject.isStatic;
                var selectedObjects = GetSelectedObjectsAndCurrent();
                var changeMode = AskChangeModeIfNecessary(selectedObjects,
                    Preferences.staticAskMode.Value, "Change Static Flags",
                    $"Do you want to {(!isStatic ? "enable" : "disable")} the static flags for all child objects as well?");

                switch (changeMode)
                {
                    case ChildrenChangeMode.ObjectOnly:
                        foreach (var obj in selectedObjects)
                        {
                            Undo.RegisterCompleteObjectUndo(obj, "Static Flags Changed");

                            obj.isStatic = isStatic;
                        }

                        break;

                    case ChildrenChangeMode.ObjectAndChildren:
                        foreach (var obj in selectedObjects)
                        {
                            Undo.RegisterFullObjectHierarchyUndo(obj, "Static Flags Changed");

                            var transforms = obj.GetComponentsInChildren<Transform>(true);

                            foreach (var transform in transforms)
                            {
                                transform.gameObject.isStatic = isStatic;
                            }
                        }

                        break;
                }
            }
        }
    }
}