using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.Icon
{
    public sealed class Lock : IconBase
    {
        protected override IconPosition Side => IconPosition.All;

        public override Texture2D PreferencesPreview => Utility.GetBackground(Styles.LockToggleStyle, false);

        public override void DoGUI(Rect rect)
        {
            var locked = (EnhancedHierarchy.CurrentGameObject.hideFlags & HideFlags.NotEditable) != 0;

            using (new GUIBackgroundColor(locked
                ? Styles.BackgroundColorEnabled
                : Styles.BackgroundColorDisabled))
            {
                GUI.changed = false;

                GUI.Toggle(rect, locked, Styles.LockContent, Styles.LockToggleStyle);

                if (!GUI.changed)
                {
                    return;
                }

                var selectedObjects = GetSelectedObjectsAndCurrent();
                var changeMode = AskChangeModeIfNecessary(selectedObjects,
                    Preferences.lockAskMode.Value, "Lock Object",
                    $"Do you want to {(!locked ? "lock" : "unlock")} the children objects as well?");

                switch (changeMode)
                {
                    case ChildrenChangeMode.ObjectOnly:
                        foreach (var obj in selectedObjects)
                        {
                            Undo.RegisterCompleteObjectUndo(obj, locked ? "Unlock Object" : "Lock Object");
                        }

                        foreach (var obj in selectedObjects)
                        {
                            if (!locked)
                            {
                                Utility.LockObject(obj);
                            }
                            else
                            {
                                Utility.UnlockObject(obj);
                            }
                        }

                        break;

                    case ChildrenChangeMode.ObjectAndChildren:
                        foreach (var obj in selectedObjects)
                        {
                            Undo.RegisterFullObjectHierarchyUndo(obj, locked ? "Unlock Object" : "Lock Object");
                        }

                        foreach (var t in selectedObjects.SelectMany(x =>
                            x.GetComponentsInChildren<Transform>(true)))
                        {
                            if (!locked)
                            {
                                Utility.LockObject(t.gameObject);
                            }
                            else
                            {
                                Utility.UnlockObject(t.gameObject);
                            }
                        }

                        break;
                }

                InternalEditorUtility.RepaintAllViews();
            }
        }
    }
}