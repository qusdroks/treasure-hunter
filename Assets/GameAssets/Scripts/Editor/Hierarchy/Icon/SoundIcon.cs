using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.Icon
{
    public sealed class SoundIcon : IconBase
    {
        private static AudioSource _audioSource;
        private static AnimBool _currentAnim;

        private static readonly Dictionary<AudioSource, AnimBool> SourcesAnim =
            new Dictionary<AudioSource, AnimBool>();

        private static Texture _icon;

        public override string Name => "Audio Source Icon";

        public override float Width
        {
            get
            {
                if (_audioSource == null || _currentAnim == null)
                {
                    return 0f;
                }

                return _currentAnim.faded * (base.Width - 2f);
            }
        }

        public override Texture2D PreferencesPreview => AssetPreview.GetMiniTypeThumbnail(typeof(AudioSource));

        static SoundIcon()
        {
            EditorApplication.update += () =>
            {
                if (!Preferences.IsButtonEnabled(new SoundIcon()))
                {
                    return;
                }

                foreach (var kvp in SourcesAnim.Where(x => x.Key && x.Value != null))
                {
                    kvp.Value.target = kvp.Key.isPlaying;
                }
            };
        }

        public override void Init()
        {
            if (!EnhancedHierarchy.IsGameObject)
            {
                return;
            }

            var comps = EnhancedHierarchy.Components;

            _audioSource = null;

            foreach (var x in comps.OfType<AudioSource>())
            {
                _audioSource = x;
                break;
            }

            if (_audioSource == null)
            {
                return;
            }

            if (SourcesAnim.TryGetValue(_audioSource, out _currentAnim))
            {
                return;
            }

            SourcesAnim[_audioSource] = _currentAnim = new AnimBool(_audioSource.isPlaying);
            _currentAnim.valueChanged.AddListener(EditorApplication.RepaintHierarchyWindow);
        }

        public override void DoGUI(Rect rect)
        {
            if (!EnhancedHierarchy.IsRepaintEvent || !EnhancedHierarchy.IsGameObject ||
                !_audioSource || Width <= 1f)
            {
                return;
            }

            using (ProfilerSample.Get())
            {
                if (!_icon)
                {
                    _icon = EditorGUIUtility.ObjectContent(null, typeof(AudioSource)).image;
                }

                rect.yMax -= 1f;
                rect.yMin += 1f;

                GUI.DrawTexture(rect, _icon, ScaleMode.StretchToFill);
            }
        }
    }
}