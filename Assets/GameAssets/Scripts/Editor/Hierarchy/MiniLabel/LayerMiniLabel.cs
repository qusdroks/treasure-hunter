using GameAssets.Scripts.Editor.Hierarchy.Icon;
using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.MiniLabel
{
    public class LayerMiniLabel : MiniLabelProvider
    {
        protected override void FillContent(GUIContent content)
        {
            content.text = EnhancedHierarchy.HasLayer
                ? LayerMask.LayerToName(EnhancedHierarchy.CurrentGameObject.layer)
                : string.Empty;
        }

        protected override bool Faded()
        {
            return EnhancedHierarchy.CurrentGameObject.layer == EnhancedHierarchy.DEFAULT_LAYER;
        }

        protected override void OnClick()
        {
        }

        public override bool Draw(Rect rect, GUIContent content, GUIStyle style)
        {
            GUI.changed = false;

            var layer = EditorGUI.LayerField(rect, EnhancedHierarchy.CurrentGameObject.layer,
                Styles.MiniLabelStyle);

            if (GUI.changed)
            {
                Layer.ChangeLayerAndAskForChildren(EnhancedHierarchy.GetSelectedObjectsAndCurrent(), layer);
            }

            return GUI.changed;
        }
    }
}