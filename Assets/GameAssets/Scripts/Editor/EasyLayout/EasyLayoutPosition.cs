﻿namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Position.
    /// </summary>
    public readonly struct EasyLayoutPosition : System.IEquatable<EasyLayoutPosition>
    {
        /// <summary>
        /// X.
        /// </summary>
        public readonly int x;

        /// <summary>
        /// Y.
        /// </summary>
        public readonly int y;

        /// <summary>
        /// Initializes a new instance of the <see cref="EasyLayoutPosition"/> struct.
        /// </summary>
        /// <param name="x">X.</param>
        /// <param name="y">Y.</param>
        public EasyLayoutPosition(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"[{x}, {y}]";
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            return obj is EasyLayoutPosition elp && Equals(elp);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="other">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        public bool Equals(EasyLayoutPosition other)
        {
            return x == other.x && y == other.y;
        }

        /// <summary>
        /// Hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return x ^ y;
        }

        /// <summary>
        /// Compare specified instances.
        /// </summary>
        /// <param name="left">Left instance.</param>
        /// <param name="right">Right instances.</param>
        /// <returns>true if the instances are equal; otherwise, false.</returns>
        public static bool operator ==(EasyLayoutPosition left, EasyLayoutPosition right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Compare specified instances.
        /// </summary>
        /// <param name="left">Left instance.</param>
        /// <param name="right">Right instances.</param>
        /// <returns>true if the instances are now equal; otherwise, false.</returns>
        public static bool operator !=(EasyLayoutPosition left, EasyLayoutPosition right)
        {
            return !left.Equals(right);
        }
    }
}