﻿using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Compact layout group.
    /// </summary>
    public class EasyLayoutCompact : EasyLayoutCompactOrGrid
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EasyLayoutCompact"/> class.
        /// </summary>
        /// <param name="layout">Layout.</param>
        public EasyLayoutCompact(EasyLayout layout)
            : base(layout)
        {
        }

        /// <summary>
        /// Group the specified elements.
        /// </summary>
        protected override void Group()
        {
            if (elementsGroup.Count == 0)
            {
                return;
            }

            if (layout.IsHorizontal)
            {
                GroupHorizontal();
            }
            else
            {
                GroupVertical();
            }

            var rows = elementsGroup.Rows;
            var columns = elementsGroup.Columns;

            switch (layout.CompactConstraint)
            {
                case CompactConstraint.MaxRowCount when rows > layout.ConstraintCount:
                    elementsGroup.Clear();
                    GroupByRows();
                    break;

                case CompactConstraint.MaxColumnCount when columns > layout.ConstraintCount:
                    elementsGroup.Clear();
                    GroupByColumns();
                    break;
            }

            if (!layout.TopToBottom)
            {
                elementsGroup.BottomToTop();
            }

            if (layout.RightToLeft)
            {
                elementsGroup.RightToLeft();
            }
        }

        private void GroupHorizontal()
        {
            var baseLength = layout.MainAxisSize;
            var length = baseLength;
            var spacing = layout.Spacing.x;

            var row = 0;
            var column = 0;

            for (var i = 0; i < elementsGroup.Count; i++)
            {
                var element = elementsGroup[i];

                if (column == 0)
                {
                    length -= element.AxisSize;
                }
                else if (length >= element.AxisSize + spacing)
                {
                    length -= element.AxisSize + spacing;
                }
                else
                {
                    length = baseLength - element.AxisSize;

                    row++;
                    column = 0;
                }

                elementsGroup.SetPosition(i, row, column);
                column++;
            }
        }

        private void GroupVertical()
        {
            var baseLength = layout.MainAxisSize;
            var length = baseLength;
            var spacing = layout.Spacing.y;

            var row = 0;
            var column = 0;

            for (var i = 0; i < elementsGroup.Count; i++)
            {
                var element = elementsGroup[i];

                if (row == 0)
                {
                    length -= element.AxisSize;
                }
                else if (length >= element.AxisSize + spacing)
                {
                    length -= element.AxisSize + spacing;
                }
                else
                {
                    length = baseLength - element.AxisSize;

                    column++;
                    row = 0;
                }

                elementsGroup.SetPosition(i, row, column);

                row++;
            }
        }

        /// <summary>
        /// Calculate sizes of the elements.
        /// </summary>
        protected override void CalculateSizes()
        {
            if (layout.ChildrenWidth == ChildrenSize.ShrinkOnOverflow &&
                layout.ChildrenHeight == ChildrenSize.ShrinkOnOverflow)
            {
                ShrinkOnOverflow();
            }
            else
            {
                if (layout.IsHorizontal)
                {
                    switch (layout.ChildrenWidth)
                    {
                        case ChildrenSize.FitContainer:
                            ResizeWidthToFit(false);
                            break;

                        case ChildrenSize.SetPreferredAndFitContainer:
                            ResizeWidthToFit(true);
                            break;

                        case ChildrenSize.ShrinkOnOverflow:
                            ShrinkWidthToFit();
                            break;
                    }

                    switch (layout.ChildrenHeight)
                    {
                        case ChildrenSize.FitContainer:
                            ResizeRowHeightToFit(false);
                            break;

                        case ChildrenSize.SetPreferredAndFitContainer:
                            ResizeRowHeightToFit(true);
                            break;

                        case ChildrenSize.ShrinkOnOverflow:
                            ShrinkRowHeightToFit();
                            break;
                    }
                }
                else
                {
                    switch (layout.ChildrenWidth)
                    {
                        case ChildrenSize.FitContainer:
                            ResizeColumnWidthToFit(false);
                            break;

                        case ChildrenSize.SetPreferredAndFitContainer:
                            ResizeColumnWidthToFit(true);
                            break;

                        case ChildrenSize.ShrinkOnOverflow:
                            ShrinkColumnWidthToFit();
                            break;
                    }

                    switch (layout.ChildrenHeight)
                    {
                        case ChildrenSize.FitContainer:
                            ResizeHeightToFit(false);
                            break;

                        case ChildrenSize.SetPreferredAndFitContainer:
                            ResizeHeightToFit(true);
                            break;

                        case ChildrenSize.ShrinkOnOverflow:
                            ShrinkHeightToFit();
                            break;
                    }
                }
            }
        }

        private void ResizeHeightToFit(bool increaseOnly)
        {
            var height = layout.InternalSize.y;

            for (var column = 0; column < elementsGroup.Columns; column++)
            {
                ResizeToFit(height, elementsGroup.GetColumn(column), layout.Spacing.y,
                    RectTransform.Axis.Vertical, increaseOnly);
            }
        }

        private void ShrinkHeightToFit()
        {
            var height = layout.InternalSize.y;

            for (var column = 0; column < elementsGroup.Columns; column++)
            {
                ShrinkToFit(height, elementsGroup.GetColumn(column), layout.Spacing.y,
                    RectTransform.Axis.Vertical);
            }
        }

        /// <summary>
        /// Calculate size of the group.
        /// </summary>
        /// <param name="horizontal">ElementsGroup are in horizontal order?</param>
        /// <param name="spacing">Spacing.</param>
        /// <param name="padding">Padding,</param>
        /// <returns>Size.</returns>
        protected override Vector2 CalculateGroupSize(bool horizontal, Vector2 spacing,
            Vector2 padding)
        {
            return elementsGroup.Size(spacing, padding);
        }

        /// <summary>
        /// Get aligned width.
        /// </summary>
        /// <param name="element">Element.</param>
        /// <param name="maxWidth">Maximum width.</param>
        /// <param name="cellMaxSize">Max size of the cell.</param>
        /// <param name="emptyWidth">Width of the empty space.</param>
        /// <returns>Aligned width.</returns>
        protected override Vector2 GetAlignByWidth(LayoutElementInfo element, float maxWidth,
            Vector2 cellMaxSize, float emptyWidth)
        {
            return new Vector2(emptyWidth * RowAligns[(int) layout.RowAlign],
                (cellMaxSize.y - element.Height) * InnerAligns[(int) layout.InnerAlign]);
        }

        /// <summary>
        /// Get aligned height.
        /// </summary>
        /// <param name="element">Element.</param>
        /// <param name="maxHeight">Maximum height.</param>
        /// <param name="cellMaxSize">Max size of the cell.</param>
        /// <param name="emptyHeight">Height of the empty space.</param>
        /// <returns>Aligned height.</returns>
        protected override Vector2 GetAlignByHeight(LayoutElementInfo element, float maxHeight,
            Vector2 cellMaxSize, float emptyHeight)
        {
            return new Vector2((cellMaxSize.x - element.Width) * InnerAligns[(int) layout.InnerAlign],
                emptyHeight * RowAligns[(int) layout.RowAlign]);
        }
    }
}