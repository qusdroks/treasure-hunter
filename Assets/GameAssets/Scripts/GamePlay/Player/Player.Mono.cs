using System.Linq;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model.Item;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Player
{
    public partial class Player
    {
        protected override void Awake()
        {
            base.Awake();

            if (Instance == null)
            {
                Instance = this;
            }

            _hook = (Weapon) GameManager.ResourceItem(GameManager.User.Hook);
            _line = (Weapon) GameManager.ResourceItem(GameManager.User.Line);
            _skin = (Weapon) GameManager.ResourceItem(GameManager.User.Skin);

            if (GameManager.User.Pet != null)
            {
                _pet = (Weapon) GameManager.ResourceItem(GameManager.User.Pet);
            }

            if (_hook != null)
            {
                hookSprite.sprite = _hook.icon;
                hook.AddComponent<PolygonCollider2D>().isTrigger = true;
                stat.atk += GameManager.User.Damage + _hook.damageToBoss +
                            GameManager.User.Damage * _hook.percentDamageToBoss / 100;
            }

            if (_line != null)
            {
                lineSprite.sprite = _line.iconEquip;
                lineRenderer.material = _line.material;
                stat.atk += _line.damageToBoss + GameManager.User.Damage * _line.percentDamageToBoss / 100;
            }

            if (_skin != null)
            {
                skinSprite.sprite = _skin.icon;
                stat.atk += _skin.damageToBoss + GameManager.User.Damage * _skin.percentDamageToBoss / 100;

                if (_skin.special.items.Count > 0)
                {
                    foreach (var x in _skin.special.items.TakeWhile(x =>
                        (_hook == null || x.id == _hook.id) && (_line == null || x.id == _line.id)))
                    {
                        if (_pet != null)
                        {
                            if (x.id != _pet.id)
                            {
                                break;
                            }
                        }

                        HasSpecial = true;
                    }

                    if (HasSpecial)
                    {
                        stat.atk += _skin.special.damageToBoss + _skin.special.percentDamageToBoss / 100;
                    }
                }
            }

            if (_pet == null)
            {
                return;
            }

            petSprite.sprite = _pet.icon;
            stat.atk += _pet.damageToBoss;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (Instance == this)
            {
                Instance = null;
            }
        }
    }
}