using System;
using System.Collections.Generic;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.GamePlay.Slot;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model;
using GameAssets.Scripts.Model.Item;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Inventory
{
    public partial class Inventory : BaseSingleton<Inventory>
    {
        [SerializeField] private List<Filter> filters = new List<Filter>();

        public Slot.Slot Selected { get; set; }
        public Page CurrentPage { get; private set; }

        private int _index;

        private void DrawFilter(Filter filter)
        {
            var items = new List<ItemData>();

            foreach (var x in filters)
            {
                if (x.filterType == filter.filterType)
                {
                    x.pageFilter.gameObject.SetActive(true);

                    CurrentPage = x.pageFilter;
                }
                else
                {
                    x.pageFilter.gameObject.SetActive(false);
                }
            }

            if (GameManager.User == null)
            {
                return;
            }

            foreach (var x in GameManager.User.Items)
            {
                var y = GameManager.ResourceItem(x);

                if (filter.filterType != FilterType.None)
                {
                    if (string.Equals(y.itemType.ToString(), filter.filterType.ToString(),
                        StringComparison.CurrentCultureIgnoreCase))
                    {
                        items.Add(x);
                    }
                }
                else
                {
                    switch (y.itemType)
                    {
                        case ItemType.Skin:
                        case ItemType.Hook:
                        case ItemType.Line:
                            break;

                        case ItemType.Pet:
                        case ItemType.Treasure:
                        case ItemType.Piece:
                        case ItemType.Booster:
                        case ItemType.Craft:
                        case ItemType.Gold:
                        case ItemType.Stone:
                        case ItemType.Diamond:
                        case ItemType.Energy:
                        case ItemType.Other:
                            items.Add(x);
                            break;
                    }
                }
            }

            _index = 0;

            filter.pageFilter.SetTotalItem(items.Count);

            if (items.Count > 0)
            {
                filter.pageFilter.OnCreateSlot(go =>
                {
                    var item = items[_index];

                    go.GetComponent<InventorySlot>().Initialize(item);

                    _index++;
                    return item.Id;
                });
            }
        }
    }
}