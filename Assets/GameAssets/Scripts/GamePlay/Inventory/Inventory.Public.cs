﻿namespace GameAssets.Scripts.GamePlay.Inventory
{
    public partial class Inventory
    {
        public void Select(Slot.Slot slot)
        {
            if (Selected != null)
            {
                Selected.Select(false);
                Selected = null;
            }

            Selected = slot;
            Selected.Select(true);
        }
    }
}