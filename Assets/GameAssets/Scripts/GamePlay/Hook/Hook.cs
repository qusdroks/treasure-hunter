﻿using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.GamePlay.Resource.Boss.Range;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.UI.UIChallenge;
using GameAssets.Scripts.UI.UIPlay.GamePlay;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Hook
{
    public partial class Hook : MonoBehaviour
    {
        [SerializeField] private Line line;

        [SerializeField] private Transform target;
        [SerializeField] private Transform exitPoint;

        [SerializeField] private float speed;
        [SerializeField] private float timerUseStrength = 7f;

        public BossRange BossRange { get; private set; }

        public bool DisputedAfterCollision { get; private set; }

        private Rigidbody2D _rb;
        private SpriteRenderer _renderer;
        private UIPlaySgp _uiPlaySgp;
        private Vector2 _velocity;

        private float _speedOrigin;
        private float _speed;
        private float _originalY;
        private float _timerUseStrength;

        private bool _used;

        private void DoShoot()
        {
            if (line.hookAction != HookAction.Rotation)
            {
                return;
            }

            var t = transform;
            var p = t.position;
            var tp = target.position;

            _speed = speed;

            GameEvent.DoChangeSpeed(Player.Player.Instance.MoreFlySpeed(), true);

            line.hookAction = HookAction.Shoot;
            _velocity = new Vector2(p.x - tp.x, p.y - tp.y);

            _velocity.Normalize();
        }

        private void DoRotation()
        {
            var t = transform;
            var lp = t.localPosition;

            if (line.hookAction == HookAction.Rotation)
            {
                lp = new Vector3(lp.x, _originalY, lp.z);
                t.localPosition = lp;
                return;
            }

            Resource = null;
            _speed = speed;

            if (_speed < 0f)
            {
                _speed *= -1;
            }

            lp = new Vector3(lp.x, _originalY, lp.z);
            t.localPosition = lp;
            line.hookAction = HookAction.Rotation;
        }

        private void CheckVisible()
        {
            if (line.hookAction == HookAction.Shoot && !_renderer.IsVisibleCamera())
            {
                LineBack();
            }
        }

        private bool UseBomb()
        {
            if (DisputedAfterCollision)
            {
                GameEvent.DoDispute(1f, true);
                UIManager.Instance.HideUI<UIChallenge>();
                GameManager.Instance.SetGameState(GameState.Playing);

                LineBack();
                return true;
            }

            if (Resource == null)
            {
                return false;
            }

            if (Resource.IsDisputing)
            {
                GameEvent.DoDispute(2f, true);
                UIManager.Instance.HideUI<UIChallenge>();
            }
            else
            {
                DestroyItem(0f);
            }

            return true;
        }

        private bool UseFirecracker()
        {
            if (DisputedAfterCollision)
            {
                GameEvent.DoDispute(1f, true);
                UIManager.Instance.HideUI<UIChallenge>();
                GameManager.Instance.SetGameState(GameState.Playing);

                LineBack();
                return true;
            }

            if (Resource == null)
            {
                return false;
            }

            if (Resource.IsDisputing)
            {
                GameEvent.DoDispute(1f, true);
                UIManager.Instance.HideUI<UIChallenge>();
            }
            else
            {
                DestroyItem(0f);
            }

            return true;
        }

        private void DoChangeSpeed(float percent, bool add)
        {
            var more = _speed * percent / 100;

            /*Debug.Log(_speed+","+more+","+percent);*/

            if (add)
            {
                _speed += more;
            }
            else
            {
                _speed = more;
            }
        }

        private void DoChangeOriginSpeed(float percent)
        {
            _timerUseStrength = timerUseStrength;
            speed += speed * percent / 100;
        }
    }
}