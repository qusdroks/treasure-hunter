﻿using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Hook
{
    public partial class Hook
    {
        public Line Line => line;
        public Vector3 ExitPoint => new Vector3(exitPoint.position.x, exitPoint.position.y, -0.01f);

        public Resource.Resource Resource { get; private set; }

        public void FailedDispute()
        {
            Resource = null;
            _speed = speed;

            if (_speed < 0f)
            {
                _speed *= -1;
            }
        }

        public void LineBack()
        {
            DisputedAfterCollision = false;
            line.hookAction = HookAction.Rewind;

            if (_speed > 0f)
            {
                _speed *= -1;
            }
        }

        public void GetResource(Resource.Resource resource, float percent)
        {
            Resource = resource;

            if (Player.Player.Instance.CanDestroyItem() && !_used)
            {
                if (Resource != null && Resource.ResourceItem != null &&
                    !Resource.ResourceItem.itemType.Equals(ItemType.Treasure))
                {
                    _uiPlaySgp.SetDestroyItem(true);
                }
            }

            var spd = percent + Player.Player.Instance.MorePullSpeed();

            if (resource != null && resource.ResourceItem != null &&
                resource.ResourceItem.itemType.Equals(ItemType.Stone))
            {
                spd += Player.Player.Instance.MoreStoneSpeed();
            }
            
            GameEvent.DoChangeSpeed(spd, false);

            LineBack();
        }

        public void DestroyItem(float percent, bool animate = true)
        {
            if (percent > 0f && Resource != null && Resource.ResourceItem != null && Resource.IsDisputing)
            {
                return;
            }

            if (animate)
            {
                SpawnerEx.CreateFxBomb(Resource.transform.position);
            }

            if (percent > 0f)
            {
                if (Resource != null && Resource.ResourceItem != null)
                {
                    _used = true;

                    _uiPlaySgp.UpdateGold(Resource.ResourceItem.value
                                          * Player.Player.Instance.MorePercentDestroyItem()
                                          / 100);
                }
            }

            _uiPlaySgp.SetDestroyItem(false);
            Resource.Disable();

            Resource = null;
            _speed = speed;

            if (_speed > 0f)
            {
                _speed *= -1;
            }

            line.hookAction = HookAction.Rewind;
        }
    }
}