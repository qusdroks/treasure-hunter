using System;
using DG.Tweening;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class Bullet : MonoBehaviour
    {
        private Animator _animator;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        public void Initiation(Transform target, Action onCompleted, Action onUpdate)
        {
            transform.DOMove(target.position, 0.075f)
                .OnUpdate(() =>
                {
                    if (target == null || target.gameObject.activeInHierarchy)
                    {
                        return;
                    }

                    SpawnerEx.DestroySpawner(transform);

                    onUpdate?.Invoke();
                })
                .OnComplete(() =>
                {
                    if (!target.gameObject.activeSelf)
                    {
                        return;
                    }

                    transform.position = target.position;

                    transform.SetParent(target);

                    onCompleted();
                });
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Resource"))
            {
                return;
            }

            var resource = other.GetComponent<Resource.Resource>();

            if (resource == null || resource.Hook == null)
            {
                return;
            }

            _animator.ChangeState(AnimationType.Attack);
            _animator.OnComplete(() => resource.Hook.DestroyItem(0f, false));
        }
    }
}