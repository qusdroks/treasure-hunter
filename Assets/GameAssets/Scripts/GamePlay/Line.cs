﻿using DG.Tweening;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Game;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class Line : BaseSingleton<Line>
    {
        [SerializeField] private Transform rotate;
        [SerializeField] private Transform skin;
        [SerializeField] private Transform hook;

        [SerializeField] private float rotationSpeed = 5f;
        [SerializeField] private float rotationAngle = 70f;

        [ReadOnly] public HookAction hookAction;

        public Transform Skin => skin;

        private LineRenderer _line;

        private float _angle;

        private void Destroy()
        {
            rotate.DOKill();
        }

        protected override void Awake()
        {
            base.Awake();

            _line = GetComponentInChildren<LineRenderer>();

            GameEvent.OnHookShoot += Destroy;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            GameEvent.OnHookShoot -= Destroy;
        }

        public override void PreInnerUpdate()
        {
            if (GameManager.Instance.IsGameState(GameState.Win) || GameManager.Instance.IsGameState(GameState.Lose))
            {
                UpdateLine();
            }
        }

        public override void InnerUpdate()
        {
            UpdateLine();
        }

        public override void InnerFixedUpdate()
        {
            if (hookAction != HookAction.Rotation)
            {
                return;
            }

            _angle += rotationSpeed;

            if (_angle > rotationAngle || _angle < -rotationAngle)
            {
                rotationSpeed *= -1;
            }

            rotate.DOLocalRotate(new Vector3(0f, 0f, _angle), 0.01f);
        }

        private void UpdateLine()
        {
            var p = _line.transform.position;

            _line.SetPosition(0, new Vector3(p.x, p.y, -0.01f));
            _line.SetPosition(1, hook.position);
        }
    }
}