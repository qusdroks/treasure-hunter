﻿using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model.Item;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Slot
{
    public class ShopSlot : Slot
    {
        [SerializeField] private Button btnBuy;

        [SerializeField] private TextMeshProUGUI txtPrice;
        [SerializeField] private TextMeshProUGUI txtSale;

        private static bool _processing;

        public override void Initialize(ItemData itemData)
        {
            base.Initialize(itemData);
            
            imgIcon.sprite = Item.icon;
            txtPrice.text = $"{Item.price}";
            txtSale.text = $"-{Random.Range(0, 30)}%";

            btnBuy.AddListener(Bought);
        }

        private async void Bought()
        {
            if (_processing)
            {
                SpawnerEx.CreateNotification("Đang xử lý mua item khác");
                return;
            }

            if (!GameManager.CheckSpentDiamond(Item.price))
            {
                SpawnerEx.CreateNotification("Bạn không có đủ kim cương");
                return;
            }

            _processing = true;

            GameManager.User.AddItem(Item, 1);

            await GameManager.UpdateUser(() =>
            {
                GameManager.User.RemoveItem(Item, 1);
                SpawnerEx.CreateNotification("Mua thất bại");
            }, BoughtSuccess);

            _processing = false;
        }

        private void BoughtSuccess()
        {
            SpawnerEx.CreateNotification($"Bạn đã mua {Item.description}");
            GameEvent.DoUpdateResource();

            btnBuy.RemoveListener();
            txtPrice.text = "Đã mua";
        }
    }
}