using GameAssets.Scripts.Model.Item;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Slot
{
    public class RewardSlot : Slot
    {
        [SerializeField] private TextMeshProUGUI txtName;

        public override void Initialize(ItemData itemData)
        {
            base.Initialize(itemData);

            imgIcon.sprite = Item.icon;
            txtName.text = Item.name;
        }
    }
}
