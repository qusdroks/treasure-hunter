﻿using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model.Item;

namespace GameAssets.Scripts.GamePlay.Slot
{
    public class ExchangeSlot : Slot
    {
        private Inventory.Inventory _inventory;

        private void Awake()
        {
            _inventory = GetComponentInParent<Inventory.Inventory>();
        }

        public override void Initialize(ItemData itemData)
        {
            base.Initialize(itemData);

            imgIcon.sprite = GameManager.ResourceItem(itemData).icon;

            GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() =>
            {
                _inventory.Select(this);
            });
        }
    }
}