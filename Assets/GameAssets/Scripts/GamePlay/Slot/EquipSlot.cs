using System;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model.Item;
using GameAssets.Scripts.UI.UIInventory;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.GamePlay.Slot
{
    public class EquipSlot : Slot, IDropHandler
    {
        [SerializeField] private ItemType itemType;

        [SerializeField] private Transform starParent;

        [SerializeField] private Image imgLock;

        [SerializeField] private TextMeshProUGUI txtUpgrade;

        private UIInventory _uiInventory;
        private ItemData _itemData;

        private void Awake()
        {
            _uiInventory = UIManager.Instance.GetUI<UIInventory>();

            OnDoubleClick = () =>
            {
                if (Item == null)
                {
                    return;
                }

                GameManager.User.AddItem(Item, 1);

                switch (Item.itemType)
                {
                    case ItemType.Hook:
                        GameManager.SetHook(null);
                        break;

                    case ItemType.Line:
                        GameManager.SetLine(null);
                        break;

                    case ItemType.Pet:
                        GameManager.SetPet(null);
                        break;
                    
                    case ItemType.Skin:
                        GameManager.SetSkin(null);
                        break;
                }
                
                Hide();

                _uiInventory.UpdateSlot(_itemData);
                _uiInventory.UpdateSkin();
            };
        }

        public override void Initialize(ItemData itemData)
        {
            base.Initialize(itemData);

            StarController.Instance.Initialize(starParent, Random.Range(1, 3));

            Set(true);

            _itemData = itemData;
            imgIcon.sprite = Item.icon;
        }

        public void Hide()
        {
            Set(false);
        }

        private void Set(bool value)
        {
            imgLock.gameObject.SetActive(!value);
            imgBg.gameObject.SetActive(value);
            imgIcon.gameObject.SetActive(value);
            starParent.gameObject.SetActive(value);
            txtUpgrade.gameObject.SetActive(value);
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (eventData.pointerDrag == null)
            {
                return;
            }

            var slot = eventData.pointerDrag.GetComponent<InventorySlot>();

            if (slot == null)
            {
                return;
            }

            if (slot.Item.itemType == itemType)
            {
                var tempData = slot.ItemData;
                var temp = slot.Item;

                if (Item != null)
                {
                    if (slot.ItemData.Amount > 1)
                    {
                        GameManager.User.RemoveItem(slot.Item, 1);

                        slot.UpdateItem(slot.ItemData);

                        GameManager.User.AddItem(Item, 1);
                    }
                    else
                    {
                        slot.SwapItem(Item, GameManager.User.Items.FindIndex(x => x.Id == tempData.Id));
                    }
                }
                else
                {
                    SpawnerEx.DestroySpawner(slot.transform);
                }

                Initialize(tempData);

                var w = temp as Weapon;

                if (w == null)
                {
                    return;
                }

                w.Use(w);
                _uiInventory.UpdateSkin();
            }
            else
            {
                slot.ReturnOrigin();
            }
        }
    }
}