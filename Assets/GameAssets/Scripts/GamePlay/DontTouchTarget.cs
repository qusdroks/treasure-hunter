﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameAssets.Scripts.GamePlay
{
    public class DontTouchTarget : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private Transform target;

        [SerializeField] private float bouncingSpeed = 0.25f;

        private bool _bouncing;

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_bouncing)
            {
                return;
            }

            _bouncing = true;

            target.DOScale(Vector3.one - Vector3.one / 10f, 0.1f)
                .OnComplete(() => target.DOScale(Vector3.one, bouncingSpeed)
                    .SetEase(Ease.OutBounce)
                    .OnComplete(() => _bouncing = false));
        }
    }
}