using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GamePlay
{
    public class ImageItem : MonoBehaviour
    {
        [SerializeField] [ReadOnly] private SpriteRenderer spriteRenderer;
        [SerializeField] [ReadOnly] private Image image;

        public void SetupSprite(Sprite icon, int layer = 99)
        {
            spriteRenderer.sprite = icon;
            spriteRenderer.sortingOrder = layer;
        }

        public void SetupImage(Sprite icon)
        {
            image.sprite = icon;
        }

        [Button]
        public void Initialize()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            image = GetComponent<Image>();
        }
    }
}