﻿using DG.Tweening;
using GameAssets.Scripts.Ex;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class BulletGreen : MonoBehaviour
    {
        private Transform _target;

        public void Initialize(Transform target)
        {
            _target = target;
        }

        public void Destroy()
        {
            var t = transform;

            t.DOKill();

            SpawnerEx.DestroySpawner(t);
        }

        private void Update()
        {
            if (gameObject != null)
            {
                transform.DOMove(_target.position, 0f);
            }
        }
    }
}