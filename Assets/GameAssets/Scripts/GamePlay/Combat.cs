using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class Combat : MonoBehaviour
    {
        [SerializeField] private TextMeshPro txtCombat;
        
        public void SetText(string description)
        {
            txtCombat.text = description;
        }
    }
}