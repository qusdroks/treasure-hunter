using DG.Tweening;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class MarkShake : MonoBehaviour
    {
        [SerializeField] private float rotationSpeed = 5f;
        [SerializeField] private float rotationAngle = 10f;

        private float _angle;

        private void Update()
        {
            _angle += rotationSpeed;

            if (_angle > rotationAngle || _angle < -rotationAngle)
            {
                rotationSpeed *= -1;
            }

            transform.DOLocalRotate(new Vector3(0f, 0f, _angle), 0.01f);
        }
    }
}