﻿using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.Manager.Game;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Resource
{
    public partial class Resource
    {
        public void SetLayer(int layer)
        {
            SpriteRenderer.sortingOrder = layer;
        }

        public void SetOriginSprite()
        {
            Snake = null;
            SnakeResultDisputeWinner = DisputeWinner.Player;
            SpriteRenderer.sprite = _originSprite;
            transform.localScale = Vector3.one / 1.5f;
        }

        public void SetSnakeSprite()
        {
            _originSprite = SpriteRenderer.sprite;
            SpriteRenderer.sprite = icSnakeCrawl;
            transform.localScale = Vector3.one / 1.5f;
        }

        public virtual void Caught()
        {
            Hook.GetResource(this, resource != null ? -resource.slowSpeed : -75f);
            transform.DOMove(Hook.ExitPoint, 0f);
            IsCaught = true;
        }

        public void Destroy()
        {
            Disable();

            GameEvent.DoHookRotation();
        }

        public void SetBulletGreen(BulletGreen bg)
        {
            bulletGreen = bg;
        }

        public void Disable()
        {
            transform.DOKill();
            gameObject.SetActive(false);

            if (bulletGreen != null)
            {
                bulletGreen.Destroy();
            }
        }

        public virtual void SuccessDispute(Vector3 position)
        {
            IsCaught = false;
            BossResultDisputeWinner = DisputeWinner.Boss;

            Hook.FailedDispute();

            transform.DOMove(position, 0.5f)
                .OnComplete(() => BossResultDisputeWinner = DisputeWinner.None)
                .AsyncWaitForCompletion()
                .AsUniTask()
                .Forget();
        }

        public virtual async UniTaskVoid Revoke()
        {
            if (received)
            {
                return;
            }

            received = true;

            if (resource != null)
            {
                switch (resource.itemType)
                {
                    case ItemType.Gold:
                    case ItemType.Energy:
                    case ItemType.Diamond:
                    case ItemType.Stone:
                        uiPlaySgp.UpdateResource(transform.position, resource).Forget();
                        break;

                    default:
                        if (resource.clip != null)
                        {
                            AudioManager.Instance.PlaySound(resource.clip.name);
                        }

                        GameEvent.DoQuestCollection(new Collection
                        {
                            id = resource.id,
                            amount = 1
                        });

                        GameManager.UpdateUser(() => { }, () => { }).Forget();
                        break;
                }

                if (Snake != null)
                {
                    Snake.GetSnake();

                    Snake = null;
                }
            }

            Destroy();
        }
    }
}