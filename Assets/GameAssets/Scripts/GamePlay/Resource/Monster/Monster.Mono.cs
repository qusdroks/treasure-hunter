﻿using DG.Tweening;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Game;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Resource.Monster
{
    public partial class Monster
    {
        protected override void Awake()
        {
            animator = GetComponent<Animator>();
            base.Awake();
        }

        private void FixedUpdate()
        {
            if (!GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            if (IsCaught)
            {
                transform.DOMove(Hook.ExitPoint, 0.05f);
            }
            else if (!idle)
            {
                Move();
            }
        }
    }
}