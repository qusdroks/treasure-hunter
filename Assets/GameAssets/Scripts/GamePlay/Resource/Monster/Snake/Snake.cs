﻿using Cysharp.Threading.Tasks;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.UI.UIChallenge;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Resource.Monster.Snake
{
    public class Snake : Monster
    {
        [SerializeField] private float decSpeed = 20;

        public float DecSpeed => decSpeed;

        private Resource _resource;

        private bool _crawl;

        protected override void Move()
        {
            if (!_crawl)
            {
                base.Move();
            }
        }

        public void GetSnake()
        {
            GameEvent.DoQuestCollection(new Collection
            {
                id = resource.id,
                amount = 1
            });

            UIManager.Instance.HideUI<UIChallenge>();
            CameraEx.StopShake();
        }

        public void SetCrawl(bool value, Resource r)
        {
            if (_resource == null)
            {
                gameObject.SetActive(false);
                _resource = r;
            }

            if (!value)
            {
                transform.position = _resource.transform.position;

                gameObject.SetActive(true);
                _resource.SetOriginSprite();

                _resource = null;
            }

            _crawl = value;
        }
    }
}