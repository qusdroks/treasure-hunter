﻿using System;
using System.Collections.Generic;
using GameAssets.Scripts.Ex;

namespace GameAssets.Scripts.Model
{
    [Serializable]
    public class UserDataSave
    {
        public int level;
        
        public float gold;
        public float diamond;
        public float death;

        public float soundVolume;
        public float musicVolume;
        
        public bool vibration;
        public bool removedAds;
    }
}