﻿using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.Model.Item
{
    [CreateAssetMenu(fileName = "Booster", menuName = "UI/Booster", order = 0)]
    public class Booster : Item
    {
        public BoosterType boosterType;
        public AudioClip clip;
        
        public float strength;
        public float lucky;
        public float timer;
    }
}