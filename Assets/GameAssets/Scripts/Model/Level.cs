﻿using Firebase.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Manager.Game;
using UnityEngine;

namespace GameAssets.Scripts.Model
{
    [CreateAssetMenu(fileName = "Level", menuName = "UI/Level", order = 0)]
    public class Level : BaseScriptableObject
    {
        public List<LevelRequire> requires = new List<LevelRequire>();
        public List<Amount> rewards = new List<Amount>();

        public AudioClip clip;
        public GameObject prefab;

        public int level;
        public int energy;
        public int time = 60;
        public int gold1;
        public int gold2;

        public bool completed;
    }

    [Serializable]
    public class LevelRequire
    {
        public Item.Item item;

        public bool equipped;

        public bool CanBypass()
        {
            if (equipped)
            {
                return item != null && (GameManager.User.Hook.Id.Equals(item.id) ||
                                        GameManager.User.Line.Id.Equals(item.id) ||
                                        GameManager.User.Pet.Id.Equals(item.id) ||
                                        GameManager.User.Skin.Id.Equals(item.id));
            }

            return GameManager.User.Items.Any(x => item != null && x.Id.Equals(item.id));
        }
    }

    [FirestoreData]
    [Serializable]
    public class LevelData
    {
        [FirestoreProperty] public string Id { get; set; }

        [FirestoreProperty] public int Level { get; set; }
        [FirestoreProperty] public int Energy { get; set; }
        [FirestoreProperty] public int Time { get; set; }

        [FirestoreProperty] public bool Completed { get; set; }

        public static LevelData Create(Level level)
        {
            return new LevelData
            {
                Id = level.id,
                Level = level.level,
                Energy = level.energy,
                Time = level.time,
                Completed = level.completed
            };
        }
    }
}