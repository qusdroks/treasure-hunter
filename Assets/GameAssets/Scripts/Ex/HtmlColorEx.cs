using UnityEngine;

namespace GameAssets.Scripts.Ex
{
    public static class HtmlColorEx
    {
        /// <summary> Color32 to HtmlColor </summary>
        public static string ToHtmlColor(this Color32 self)
        {
            return $"#{self.r:x02}{self.g:x02}{self.b:x02}{self.a:x02}";
        }

        /// <summary> Color to HtmlColor </summary>
        public static string ToHtmlColor(this Color self)
        {
            var color32 = (Color32) self;
            return $"#{color32.r:x02}{color32.g:x02}{color32.b:x02}{color32.a:x02}";
        }

        /// <summary> int(RGB) to HtmlColor </summary>
        public static string ToHtmlColor(this int self)
        {
            return $"#{(self & 0x00FF0000) >> 16:x02}{(self & 0x0000FF00) >> 8:x02}" +
                   $"{(self & 0x000000FF) >> 0:x02}ff";
        }

        /// <summary> uint(ARGB) to HtmlColor </summary>
        public static string ToHtmlColor(this uint self)
        {
            return $"#{(self & 0x00FF0000) >> 16:x02}{(self & 0x0000FF00) >> 8:x02}" +
                   $"{(self & 0x000000FF) >> 0:x02}{(self & 0xFF000000) >> 24:x02}";
        }
    }
}