using System.Text;
using GameAssets.Scripts.General;

namespace GameAssets.Scripts.Ex
{
    public class RichText
    {
        public static string Bold(object obj)
        {
            return Tag(obj, Const.BOLD);
        }

        public static string Italic(object obj)
        {
            return Tag(obj, Const.ITALIC);
        }

        public static string Size(object obj, int size)
        {
            return Tag(obj, Const.SIZE, size);
        }

        public static string Color(object obj, string color)
        {
            return Tag(obj, Const.COLOR, color);
        }

        // fixed colors

        public static string Aqua(object obj)
        {
            return Tag(obj, Const.COLOR, Const.AQUA);
        }

        public static string Black(object obj)
        {
            return Tag(obj, Const.COLOR, Const.BLACK);
        }

        public static string Blue(object obj)
        {
            return Tag(obj, Const.COLOR, Const.BLUE);
        }

        public static string Brown(object obj)
        {
            return Tag(obj, Const.COLOR, Const.BROWN);
        }

        public static string Cyan(object obj)
        {
            return Tag(obj, Const.COLOR, Const.CYAN);
        }

        public static string DarkBlue(object obj)
        {
            return Tag(obj, Const.COLOR, Const.DARK_BLUE);
        }

        public static string Green(object obj)
        {
            return Tag(obj, Const.COLOR, Const.GREEN);
        }

        public static string Grey(object obj)
        {
            return Tag(obj, Const.COLOR, Const.GREY);
        }

        public static string LightBlue(object obj)
        {
            return Tag(obj, Const.COLOR, Const.LIGHT_BLUE);
        }

        public static string Lime(object obj)
        {
            return Tag(obj, Const.COLOR, Const.LIME);
        }

        public static string Magenta(object obj)
        {
            return Tag(obj, Const.COLOR, Const.MAGENTA);
        }

        public static string Maroon(object obj)
        {
            return Tag(obj, Const.COLOR, Const.MAROON);
        }

        public static string Navy(object obj)
        {
            return Tag(obj, Const.COLOR, Const.NAVY);
        }

        public static string Olive(object obj)
        {
            return Tag(obj, Const.COLOR, Const.OLIVE);
        }

        public static string Orange(object obj)
        {
            return Tag(obj, Const.COLOR, Const.ORANGE);
        }

        public static string Purple(object obj)
        {
            return Tag(obj, Const.COLOR, Const.PURPLE);
        }

        public static string Red(object obj)
        {
            return Tag(obj, Const.COLOR, Const.RED);
        }

        public static string Silver(object obj)
        {
            return Tag(obj, Const.COLOR, Const.SILVER);
        }

        public static string Teal(object obj)
        {
            return Tag(obj, Const.COLOR, Const.TEAL);
        }

        public static string White(object obj)
        {
            return Tag(obj, Const.COLOR, Const.WHITE);
        }

        public static string Yellow(object obj)
        {
            return Tag(obj, Const.COLOR, Const.YELLOW);
        }

        private static string Tag(object obj, string tag, object attribute = null)
        {
            var sb = new StringBuilder();

            if (obj == null)
            {
                return sb.ToString();
            }

            var text = obj.ToString();
            var texts = text.Split('\n');

            if (attribute == null)
            {
                sb.Append("<").Append(tag).Append(">").Append(texts[0])
                    .Append("</").Append(tag).Append(">");

                for (var i = 1; i < texts.Length; ++i)
                {
                    sb.Append("\n<").Append(tag).Append(">").Append(texts[i])
                        .Append("</").Append(tag).Append(">");
                }
            }
            else
            {
                sb.Append("<").Append(tag).Append("=").Append(attribute)
                    .Append(">").Append(texts[0])
                    .Append("</").Append(tag).Append(">");

                for (var i = 1; i < texts.Length; ++i)
                {
                    sb.Append("\n<").Append(tag).Append("=").Append(attribute).Append(">")
                        .Append(texts[i]).Append("</").Append(tag).Append(">");
                }
            }

            return sb.ToString();
        }


        public class OpenTags
        {
            public static string bold = $"<{Const.BOLD}>";
            public static string italic = $"<{Const.ITALIC}>";
            public static string size = $"<{Const.SIZE}=";
            public static string color = $"<{Const.COLOR}=";
        }

        public class CloseTags
        {
            public static string bold = $"</{Const.BOLD}>";
            public static string italic = $"</{Const.ITALIC}>";
            public static string size = $"</{Const.SIZE}>";
            public static string color = $"</{Const.COLOR}>";
        }
    }
}