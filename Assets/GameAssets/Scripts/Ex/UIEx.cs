﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.Manager.Game;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameAssets.Scripts.Ex
{
    public static class UIEx
    {
        private static readonly int MainTex = Shader.PropertyToID("_MainTex");
        private static readonly int Progress = Shader.PropertyToID("_Progress");

        #region Rect

        public static RectTransform SetFullSize(this RectTransform self)
        {
            self.sizeDelta = new Vector2(0.0f, 0.0f);
            self.anchorMin = new Vector2(0.0f, 0.0f);
            self.anchorMax = new Vector2(1.0f, 1.0f);
            self.pivot = new Vector2(0.5f, 0.5f);
            return self;
        }

        public static Vector2 GetSize(this RectTransform self)
        {
            return self.rect.size;
        }

        public static void SetSize(this RectTransform self, Vector2 newSize)
        {
            var pivot = self.pivot;
            var dist = newSize - self.rect.size;

            self.offsetMin -= new Vector2(dist.x * pivot.x, dist.y * pivot.y);
            self.offsetMax += new Vector2(dist.x * (1f - pivot.x), dist.y * (1f - pivot.y));
        }

        public static RectTransform SetSizeFromLeft(this RectTransform self, float rate)
        {
            self.SetFullSize();

            var width = self.rect.width;

            self.anchorMin = new Vector2(0.0f, 0.0f);
            self.anchorMax = new Vector2(0.0f, 1.0f);
            self.pivot = new Vector2(0.0f, 1.0f);
            self.sizeDelta = new Vector2(width * rate, 0.0f);

            return self;
        }

        public static RectTransform SetSizeFromRight(this RectTransform self, float rate)
        {
            self.SetFullSize();

            var width = self.rect.width;

            self.anchorMin = new Vector2(1.0f, 0.0f);
            self.anchorMax = new Vector2(1.0f, 1.0f);
            self.pivot = new Vector2(1.0f, 1.0f);
            self.sizeDelta = new Vector2(width * rate, 0.0f);

            return self;
        }

        public static RectTransform SetSizeFromTop(this RectTransform self, float rate)
        {
            self.SetFullSize();

            var height = self.rect.height;

            self.anchorMin = new Vector2(0.0f, 1.0f);
            self.anchorMax = new Vector2(1.0f, 1.0f);
            self.pivot = new Vector2(0.0f, 1.0f);
            self.sizeDelta = new Vector2(0.0f, height * rate);

            return self;
        }

        public static RectTransform SetSizeFromBottom(this RectTransform self, float rate)
        {
            self.SetFullSize();

            var height = self.rect.height;

            self.anchorMin = new Vector2(0.0f, 0.0f);
            self.anchorMax = new Vector2(1.0f, 0.0f);
            self.pivot = new Vector2(0.0f, 0.0f);
            self.sizeDelta = new Vector2(0.0f, height * rate);

            return self;
        }

        public static bool InScreenRect(this RectTransform self, Vector2 screenPos)
        {
            var canvas = self.GetComponentInParent<Canvas>();

            switch (canvas.renderMode)
            {
                case RenderMode.ScreenSpaceCamera:
                {
                    var camera = canvas.worldCamera;

                    if (camera != null)
                    {
                        return RectTransformUtility.RectangleContainsScreenPoint(self, screenPos, camera);
                    }
                }
                    break;

                case RenderMode.ScreenSpaceOverlay:
                    return RectTransformUtility.RectangleContainsScreenPoint(self, screenPos);

                case RenderMode.WorldSpace:
                    return RectTransformUtility.RectangleContainsScreenPoint(self, screenPos);
            }

            return false;
        }

        public static bool InScreenRect(this RectTransform self, RectTransform rectTransform)
        {
            var rect1 = GetScreenRect(self);
            var rect2 = GetScreenRect(rectTransform);
            return rect1.Overlaps(rect2);
        }

        public static Rect GetScreenRect(this RectTransform self)
        {
            var rect = new Rect();
            var canvas = self.GetComponentInParent<Canvas>();
            var camera = canvas.worldCamera;

            if (camera == null)
            {
                return rect;
            }

            var corners = new Vector3[4];
            self.GetWorldCorners(corners);

            rect.min = camera.WorldToScreenPoint(corners[0]);
            rect.max = camera.WorldToScreenPoint(corners[2]);

            return rect;
        }

        #endregion

        #region Void

        public static async void SceneTransition(this Image img, Sprite icTransition,
            float speed = 1f, Action onStart = null, Action onComplete = null)
        {
            onStart?.Invoke();
            img.material.SetTexture(MainTex, icTransition.texture);

            while (img.material.GetFloat(Progress) < 1.1f)
            {
                img.material.SetFloat(Progress,
                    Mathf.MoveTowards(img.material.GetFloat(Progress),
                        1.1f, speed * Time.deltaTime));

                await UniTask.Yield();
            }

            onComplete?.Invoke();
        }

        public static void OpenClose(this CanvasGroup cg)
        {
            cg.alpha = cg.alpha > 0 ? 0 : 1;
            cg.blocksRaycasts = cg.blocksRaycasts != true;
        }

        public static void SetActive(this ParticleSystem ps, bool active = true)
        {
            if (active)
            {
                ps.gameObject.SetActive(true);
                ps.Play();
            }
            else
            {
                var main = ps.main;

                main.stopAction = ParticleSystemStopAction.Disable;

                ps.IgnoreRaycast();
                ps.Stop();
            }
        }

        public static async void NumberCounter(this TextMeshProUGUI txt, float start, float end,
            float duration = 2f)
        {
            var desiredNumber = start + end;
            var currentNumber = start <= 0f ? 0.001f : start;

            while (!MathfEx.Equal(currentNumber, desiredNumber))
            {
                if (start < desiredNumber)
                {
                    currentNumber += duration * Time.deltaTime * (desiredNumber - start);

                    if (currentNumber >= desiredNumber)
                    {
                        currentNumber = desiredNumber;
                    }
                }
                else
                {
                    currentNumber -= duration * Time.deltaTime * (start - desiredNumber);

                    if (currentNumber <= desiredNumber)
                    {
                        currentNumber = desiredNumber;
                    }
                }

                txt.text = $"{currentNumber:0}";

                await UniTask.Yield();
            }
        }

        public static void IgnoreRaycast(this Component c)
        {
            c.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
        }

        public static void SetLayer(this GameObject self, int layer, bool includeChildren = true)
        {
            self.layer = layer;

            if (!includeChildren)
            {
                return;
            }

            var children = self.transform.GetComponentsInChildren<Transform>(true);

            foreach (var x in children)
            {
                x.gameObject.layer = layer;
            }
        }

        /// <summary>
        /// Only applies to the case where anchor min is vector zero and anchor max is vector one
        /// </summary>
        public static void SetOffset(this RectTransform rt, float left, float right, float top, float bottom)
        {
            var min = rt.offsetMin;
            var max = rt.offsetMax;

            min += new Vector2(left - min.x, top - min.y);
            rt.offsetMin = min;

            max -= new Vector2(right + max.x, bottom + max.y);
            rt.offsetMax = max;
        }

        #endregion

        #region String

        public static string NewLine => "\u000a";

        #endregion

        #region Bool

        public static bool IsVisibleCamera(this Renderer renderer)
        {
            var planes = GeometryUtility.CalculateFrustumPlanes(GameManager.Camera);
            return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
        }

        public static bool IsPointerOverUIObject()
        {
            var ped = new PointerEventData(EventSystem.current)
            {
                position = new Vector2(Input.mousePosition.x, Input.mousePosition.y)
            };

            var results = new List<RaycastResult>();

            EventSystem.current.RaycastAll(ped, results);
            return results.Count > 0;
        }

        #endregion
    }
}